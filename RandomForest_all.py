import numpy as np
import sys

sys.path.append(r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Preprocessing')
from Last_Preprocess_Overall import Make_Features_and_Labels
from sklearn.model_selection import KFold
from sklearn.metrics import r2_score
from sklearn.preprocessing import MinMaxScaler,StandardScaler,RobustScaler
import difflib
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, WhiteKernel,DotProduct, Matern
from sklearn.datasets import load_iris #Just to check
from sklearn.metrics import mean_squared_error, mean_absolute_percentage_error
from sklearn.ensemble import RandomForestRegressor


from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import pandas as pd
import shap

import joblib




def Random_Forest(Features, Labels,Fleftout, Lleftout, names, K_folds,n_estimators,seed,patience=20, Hold_out = False, std_width=0.1, add_noise=True, factor=5, name="Copper (Cu)",reduced_input=True,LS_value=None,save_plot=False,name_leftout=None,results_folder=None,pick_inputs=None,feature_engineering=False):
    '''
    Parameters
    ----------
    Features : Features for the model
    Labels : Labels for the model
    K_folds : How many folds using kfolds
    std_width : if add_noise==True: Create new data, std_width=0.01 draws gaussian samples about 
    other values with std_dev 0.01. The default is 0.01.
    add_noise : Boolean. DEfault = True
    factor : Integer. Factor by how much should we increase data? if 3, our dataset will consist of original,
    plus 3 with added noise, making the training data 4 times as big. The default is 3.
    names: List of names in the data, like Calcium, L/S, etc;
    name: The target label. Model will only predict the name of this label.
    
    #Gaussian process: Use kernels k(x_i,x_j) during training to train hyperparameters:
        So instead of doing the linear fit with a feature transformation (\phi(x)=\tilde(x)), we consider
        a feature transformation which transforms into gaussians, and train on this instead. Makes
        computationally easy to evaluate new points, but training can take time depening on training data size.
        
    Returns
    -------
    x : TYPE
        DESCRIPTION.

    '''
    shape_1 = np.shape(Features) 
    shape_2 = np.shape(Labels)
    
    # print(shape_1,shape_2)
    print("Currently fitting for (Random Forest)): ",str(name), "Using seed: ",str(seed))

    # Define the number of folds
    k_folds = K_folds
    
    # Initialize the KFold object
    kf = KFold(n_splits=k_folds, shuffle = True, random_state=seed)
    
    # Initialize lists to store the R-squared scores for each fold
    r2_scores = []
    wmap_score=[]
    Relative_score = []
    
    r2_scores_train = []
    wmap_score_train = []
    Relative_score_train = []
    RMSE_train = []
    RMSE_test = []
    
    Relative_score_leave_out = []
    RMSE_leave_out = []
    r2_score_leave_out = []
    wmap_leave_out = []
    
    Lleftout_copy = np.array(Lleftout)
    # Convert feature matrix and labels to numpy array
    Feature_Matrix_ = np.array(Features)
    Label_Vector_ = np.array(Labels)
    
    Fleftout_copy = np.array(Fleftout)
    print(np.shape(Lleftout_copy))
    

    if LS_value is not None: #Remove the training for small LS values.
        if isinstance(LS_value, list):
            mask = ~np.isin(Label_Vector_[:,2], LS_value)  # Inverse the condition
            Leftmask = ~np.isin(Lleftout_copy[:,2], LS_value)
        else:
            mask = (Label_Vector_[:,2] == LS_value)  # Inverse the condition
            Leftmask = (Lleftout_copy[:,2] == LS_value) 
        # mask = (Label_Vector_[:,2] != LS_value)
        Feature_Matrix_ = Feature_Matrix_[mask]
        Label_Vector_ = Label_Vector_[mask]
        
        Lleftout_copy = Lleftout_copy[Leftmask]
        Fleftout_copy = Fleftout_copy[Leftmask]
    Fleftout = Fleftout_copy
    print(np.shape(Fleftout))
    
    # Initialize MinMaxScaler
    # print(np.shape(Feature_Matrix_))
    # print(np.shape(Label_Vector_))
    scaler = MinMaxScaler() #C = (c_i - c_min)/(c_max - c_min) -> Range in [0,1]
    scaler = StandardScaler()
    scaler = RobustScaler()
    folder_counter = 0
    
    scaler = MinMaxScaler() #C = (c_i - c_min)/(c_max - c_min) -> Range in [0,1]
    scaler=StandardScaler()
    # scaler=RobustScaler()
    combined_features = np.vstack((Feature_Matrix_, Fleftout_copy))
    scaled_combined_features = scaler.fit_transform(combined_features)
    Feature_Matrix_Before_Scaling = Feature_Matrix_
    Feature_Matrix_ = scaled_combined_features[:Feature_Matrix_.shape[0]]
    Fleftout_trans_1 = scaled_combined_features[Feature_Matrix_.shape[0]:]
    
    # Create DataLoader for training and testing
    if Hold_out == False:
        r2_avg_scores = []
        for i in range(patience):
            for train_index, test_index in kf.split(Feature_Matrix_):
                X_train_fold, X_test_fold = Feature_Matrix_[train_index], Feature_Matrix_[test_index]
                y_train_fold, y_test_fold = Label_Vector_[train_index], Label_Vector_[test_index]
                untransformed_train, untransformed_test = Feature_Matrix_Before_Scaling[train_index],Feature_Matrix_Before_Scaling[test_index]
                folder_counter+=1
                Fleftout_copy = Fleftout_trans_1
                #Here we should make more data from noise on the train folds. Perhaps this will give us convergence.
                Feat_list = list(X_train_fold)
                Lab_list = list(y_train_fold)
                
                if add_noise: #This adds noisy data to the testing folds, to create more to train on.
                    #Here we should make more data from noise on the train folds. Perhaps this will give us convergence.
                    Feat_list = list(X_train_fold)
                    Lab_list = list(y_train_fold)
                    m,n = np.shape(Feat_list)
                    noise_Feats = []
                    noise_Labs = []
                    for times in range(factor):
                        for row in Feat_list: #This moves row by row
                            temp_row = []
                            for value in row: #This moves along the row
                                # std_dev = np.abs(value*std_width) #This is how big the fluctuation of the data should be. i.e "how much noise".
                                # print(std_dev)
                                std_dev = std_width
                                noise = np.random.normal(value, std_dev,1)[0] #This creates some noise.
                                temp_row.append(noise)
                            noise_Feats.append(temp_row)
                        for row in Lab_list:
                            temp_row = []
                            for value in row:
                                # std_dev = np.abs(value*std_width) #This is how big the fluctuation of the data should be. i.e how much noise.
                                std_dev = std_width
                                noise = np.random.normal(value, std_dev,1)[0] #This creates some noise.
                                temp_row.append(noise)
                            
                            noise_Labs.append(temp_row)
                    Feat_list = Feat_list + noise_Feats
                    Lab_list = Lab_list + noise_Labs
                    
                    X_train_fold = np.array(Feat_list)
                    y_train_fold = np.array(Lab_list)
                
                if len(name)!=0: #Reduces output
                    close_match = difflib.get_close_matches(name + "LS01", names, n=1,cutoff = 0.6)
                    if len(close_match) == 0:
                        close_match = difflib.get_close_matches(name, names, n=1,cutoff = 0.6)
                    index = (np.where(np.array(names) == close_match)[0][0])-3 #-3 since first three are removed from labels, and names has len(features)
                
                    
                    y_train_fold = y_train_fold[:,index+3]
                    y_train_fold = y_train_fold.reshape(-1,1)
                    y_test_fold = y_test_fold[:,index+3]
                    y_test_fold = y_test_fold.reshape(-1,1)
                    shape_2 = np.shape(y_train_fold)
                    
                    Lleftout_arr = Lleftout_copy[:,index+3]
                    Lleftout_arr = Lleftout_arr.reshape(-1,1)
                    # string_to_add = "LS*"+str(name)
                    # another_string_to_add = str(name)
                    # a_string_series = pd.Series([another_string_to_add])
                    # string_series = pd.Series([string_to_add])
                    # namese = pd.concat([names, string_series])
                    # namese=namese.reset_index(drop=True)
                    # #Here we add the term "LS*Element". 
                    # X_test_fold = np.concatenate((X_test_fold, (X_test_fold[:, 2] * X_test_fold[:, index+3]).reshape(-1, 1)), axis=1)
                    
                    # X_train_fold = np.concatenate((X_train_fold, (X_train_fold[:, 2] * X_train_fold[:, index+3]).reshape(-1, 1)), axis=1)
                    
                    if reduced_input: #This will make only LS, Leitf, etc plus element of output to input.
                        # X_train_fold = np.concatenate((X_train_fold[:, :3], X_train_fold[:, index+3].reshape(-1, 1)), axis=1)
                        X_train_fold = np.concatenate((X_train_fold[:, :3], X_train_fold[:, index+3].reshape(-1, 1), (X_train_fold[:, 2] * X_train_fold[:, index+3]).reshape(-1, 1)), axis=1)
                        # X_train_fold = X_train_fold.reshape(-1,1)
                        # X_test_fold = np.concatenate((X_test_fold[:, :3], X_test_fold[:, index+3].reshape(-1, 1)), axis=1)
                        X_test_fold = np.concatenate((X_test_fold[:, :3], X_test_fold[:, index+3].reshape(-1, 1), (X_test_fold[:, 2] * X_test_fold[:, index+3]).reshape(-1, 1)), axis=1)
                        # X_test_fold = X_test_fold.reshape(-1,1)
                        
                        # namese = namese[:3]
                        # namese = pd.concat([namese,a_string_series,string_series])
                        # namese=namese.reset_index(drop=True)
                    else:
                        if feature_engineering:
                            
                            X_train_fold = np.concatenate((X_train_fold, (X_train_fold[:, 2] * X_train_fold[:, index+3]).reshape(-1, 1).reshape(-1, 1),(1/X_train_fold[:, 2] * X_train_fold[:, index+3]).reshape(-1, 1),(np.exp(-X_train_fold[:, 2] * X_train_fold[:, index+3])).reshape(-1, 1)), axis=1)
                            X_test_fold = np.concatenate((X_test_fold, (X_test_fold[:, 2] * X_test_fold[:, index+3]).reshape(-1, 1).reshape(-1, 1),(1/X_test_fold[:, 2] * X_test_fold[:, index+3]).reshape(-1, 1),(np.exp(-X_test_fold[:, 2] * X_test_fold[:, index+3])).reshape(-1, 1)), axis=1)
                            string_to_add = "LS*"+str(name)
                            string_to_add_2 = "LS/"+str(name)
                            string_to_add_3 = "exp(-"+str(name) + ')'
                            string_series = pd.Series([string_to_add,string_to_add_2,string_to_add_3])
                            namese = pd.concat([names, string_series])
                            namese=namese.reset_index(drop=True)
                            
                            Fleftout_copy = np.concatenate((Fleftout_copy, (Fleftout_copy[:, 2] * Fleftout_copy[:, index+3]).reshape(-1, 1).reshape(-1, 1),(1/Fleftout_copy[:, 2] * Fleftout_copy[:, index+3]).reshape(-1, 1),(np.exp(-Fleftout_copy[:, 2] * Fleftout_copy[:, index+3])).reshape(-1, 1)), axis=1)
                        
                        elif feature_engineering == False:
                            X_train_fold = np.concatenate((X_train_fold, (X_train_fold[:, 2] * X_train_fold[:, index+3]).reshape(-1, 1).reshape(-1, 1)), axis=1)
                            X_test_fold = np.concatenate((X_test_fold, (X_test_fold[:, 2] * X_test_fold[:, index+3]).reshape(-1, 1).reshape(-1, 1)), axis=1)
                            string_to_add = "LS*"+str(name)
                            string_series = pd.Series([string_to_add])
                            namese = pd.concat([names, string_series])
                            namese=namese.reset_index(drop=True)
                            
                            Fleftout_copy = np.concatenate((Fleftout_copy, (Fleftout_copy[:, 2] * Fleftout_copy[:, index+3]).reshape(-1, 1).reshape(-1, 1)), axis=1)
                    nameinput = name
                    if pick_inputs is not None:
                        def get_closest_indices(names, names_to_keep):
                            closest_indices = []
                            for name in names_to_keep:
                                
                                closest_match1 = difflib.get_close_matches(name, names, n=1, cutoff=1) #Exact if L/S Kumuliert, LS*Calcium, pH , etc;
                                if closest_match1:
                                    closest_indices.append(np.where(names == closest_match1[0])[0][0])
                                else:
                                    # Find the closest base name in the names array (ignoring LSX)
                                    closest_match = difflib.get_close_matches(name, names, n=3, cutoff=0.5)
                                    
                                        
                                    if name == nameinput:
                                        closest_match = difflib.get_close_matches(name, names, n=4, cutoff=0.5) #Because also LS*element exists. So there are 4.
                                        if feature_engineering:
                                            closest_match = difflib.get_close_matches(name, names, n=6, cutoff=0.5)
                                    # print("Name:", name)
                                    # print("Base Names:", base_names)
                                    
                                    if closest_match:
                                        for matches in closest_match:
                                            
                                            closest_indices.append(np.where(names == matches)[0][0])
                            return closest_indices, pick_inputs

                        indices_to_use, _ = get_closest_indices(namese, pick_inputs)
                        namese = namese[indices_to_use].reset_index(drop=True)
                        
                        
                        if folder_counter==1:
                            print("We will use: ",namese, "as our input")
                            Fleftout_copy = Fleftout_copy[:,indices_to_use]
                        X_train_fold=X_train_fold[:,indices_to_use]
                        X_test_fold = X_test_fold[:,indices_to_use]
                # namese = names
                Fleftout = Fleftout_copy
                
                X_for_plots = X_test_fold
                
                # print(np.shape(X_train_fold))
                # print(np.shape(y_train_fold))
                # print(np.shape(X_test_fold))
                # print(np.shape(y_test_fold))
                
                
                # Create GPR model
                model = RandomForestRegressor(n_estimators=n_estimators, random_state = seed)
                # Train the model
                model.fit(X_train_fold, y_train_fold)
                
                # Evaluate the model
                y_pred = model.predict(X_test_fold)
                y_pred_train = model.predict(X_train_fold)
                # Calculate the R-squared score for the fold
                r2 = r2_score(y_test_fold, y_pred)
                r2_scores.append(r2)
                
                joblib.dump(model,os.path.join(results_folder,"Random_Forest_model"+str(name_leftout)+'_'+str(name)+'_'+str(folder_counter)))
                
                Left_out_scaled = (Fleftout)
                pred_leftout = model.predict(Left_out_scaled)
                
                def wmape(actual, pred):
                    actual = np.array(actual)
                    pred = np.array(pred)
                    non_zero_indices = np.where(actual != 0)[0]
                    actual_non_zero = actual[non_zero_indices]
                    pred_non_zero = pred[non_zero_indices]
                    
                    wMAPE = np.sum(np.abs(actual_non_zero-pred_non_zero))/np.sum(np.abs(actual_non_zero))*100
                    return wMAPE
                
                def Relative_loss(actual,pred):
                    
                    actual = np.array(actual)
                    pred = np.array(pred)
                    non_zero_indices = np.where(actual != 0)[0]
                    
                    actual_non_zero = actual[non_zero_indices]
                    pred_non_zero = pred[non_zero_indices]
                    norm = len(actual_non_zero)
                    Relative_loss = np.sum(np.abs(actual_non_zero-pred_non_zero)/(np.abs(actual_non_zero)))/norm
                    return Relative_loss
            
                romana_loss = wmape(y_test_fold,y_pred)
                if folder_counter == 1:
                    if results_folder is None:
                        results_folder = os.path.join("E_Results", "Random_Forest")
                        
                    K = 5
                    background_data_summary = shap.kmeans(X_train_fold, K)
                    
                    explainer = shap.KernelExplainer(model.predict, background_data_summary)
                    shap_values = explainer.shap_values(X_test_fold)
                    shap.summary_plot(shap_values,X_test_fold, feature_names = namese,show=False)
                    X_values = X_for_plots[:,2]
                    y_values = y_test_fold
                    y_pred_values = y_pred
                    colors = plt.cm.viridis(np.linspace(0, 1, len(X_values)))
                    if LS_value is not None:
                        plt.title(r"$\bf{Random Forest}$" + "\nElement: {} , R2 value: {:.2f}, Seed: {}".format(name, r2,seed))
                    else:
                        plt.title(r"$\bf{Random Forest}$" + "\nElement: {} , R2 value: {:.2f}, Seed: {}".format(name, r2,seed))
                    if save_plot:
                        plt.savefig(os.path.join(results_folder, f"shap_summary_plot_RF_{name}_{name_leftout}.png"))
                    plt.show()
                    

                    plt.figure()
                    for i, (x_val, y_val) in enumerate(zip(X_values, y_values)):
                        plt.scatter(x_val, y_val, label="True values" if i == 0 else "", marker='s', color=colors[i])
                        
                    # plt.scatter(X_for_plots[:,2],y_test_fold, label = "True values",color='blue',marker = 's')
                    for i, (x_val, y_prd) in enumerate(zip(X_values, y_pred_values)):
                        plt.scatter(x_val, y_prd, label = "Predictions"if i == 0 else "", marker='x', color=colors[i])
                    # plt.scatter(X_for_plots[:,2],y_pred,label = "Predictions",color='red',marker = 'x')
                    if LS_value is not None:
                        plt.title(r"$\bf{Random Forest}$" + "\nElement: {} , R2 value: {:.2f}, Seed: {}".format(name, r2,seed))
                    else:
                        plt.title(r"$\bf{Random Forest}$" + "\nElement: {} , R2 value: {:.2f}, Seed: {}".format(name, r2,seed))
                        
                    plt.grid(True)
                    plt.legend()
                    plt.show()
                    
                    plt.figure()
                    Relative_loss_for_plot = Relative_loss(Lleftout_arr, pred_leftout)
                    plt.title(r"$\bf{Random \ Forest}$"+"\nElement: {} , Relative Leftout: {:.2f}, Seed: {}".format(name, Relative_loss_for_plot,seed))
                    for i, (y_val, y_prd) in enumerate(zip(y_values, y_pred_values)):
                        # plt.scatter(y_val, y_prd, label = "Plot"if i == 0 else "", marker='x', color=colors[i])
                        plt.scatter(y_val, y_prd, label = "Validation"if i == 0 else "", marker='x', color='green')
                    plt.scatter(y_train_fold,y_pred_train, label = "Train", color = 'blue', marker = '.')
                    plt.scatter(Lleftout_arr,pred_leftout,label = "Test",color = 'red',marker = 'D' )
                    plt.plot([min(y_values), max(y_values)], [min(y_values), max(y_values)], color='black', linestyle='--', linewidth=2)
                    plt.ylabel("Predicted values")
                    plt.xlabel("Actual values")
                    plt.grid(True)
                    plt.legend()
                    if save_plot:
                        plt.savefig(os.path.join(results_folder, f"Results_RF_{name}_{name_leftout}.png"))
                    plt.show()
                    print(f"Fold R-squared: {r2:.4f}")
                
                # Calculate the average R-squared score across all folds
            if Hold_out == False:
                avg_r2_score = np.mean(r2_scores)
                # print("Average R-squared score:", avg_r2_score)
                r2_avg_scores.append(avg_r2_score)
                
                
                r2 = r2_score(y_test_fold, y_pred)
                r2_scores.append(r2)
                
                romana_loss = wmape(y_test_fold,y_pred)
                wmap_score.append(romana_loss)
                Relative_score.append(Relative_loss(y_test_fold,y_pred))
                
                y_true_test = y_train_fold 
                y_pred_test = model.predict(X_train_fold)
                
                r2_test = r2_score(y_true_test,y_pred_test)
                r2_scores_train.append(r2_test)
                wmap_score_train.append(wmape(y_true_test,y_pred_test))
                Relative_score_train.append(Relative_loss(y_true_test,y_pred_test))
                
                r2_lo = r2_score(Lleftout_arr, pred_leftout)
                r2_score_leave_out.append(r2_lo)
                
                RMSE_leave_out.append(mean_squared_error(Lleftout_arr, pred_leftout))
                wmap_leave_out.append(wmape(Lleftout_arr, pred_leftout))
                Relative_score_leave_out.append(Relative_loss(Lleftout_arr, pred_leftout))
                r2_score_leave_out.append(r2_score(Lleftout_arr, pred_leftout))
                

                RMSE_train.append(mean_squared_error(y_true_test,y_pred_test))
                RMSE_test.append(mean_squared_error(y_test_fold,y_pred))
                print("Variance: ", np.var(y_test_fold))
                
                
                y_validation_true,y_validation_pred = y_test_fold,y_pred
                y_test_true,y_test_pred = y_true_test,y_pred_test
                Leftout_true, Leftout_pred = Lleftout_arr,pred_leftout
                
                return avg_r2_score,np.mean(wmap_score),np.mean(Relative_score),np.mean(RMSE_test), np.mean(r2_scores_train), np.mean(wmap_score_train), np.mean(Relative_score_train),np.mean(RMSE_train),np.mean(r2_score_leave_out), np.mean(wmap_leave_out), np.mean(Relative_score_leave_out),np.mean(RMSE_leave_out),y_validation_true,y_validation_pred,y_test_true,y_test_pred,Leftout_true, Leftout_pred
                # print("OK")
        # print("The avg:", np.max(r2_avg_scores))
        # print(mean_squared_error(y_test_fold, y_pred))
    
            
    if Hold_out == True:
        print("We dont do Hold_out==True anymore")
        return None,None,None,None,None,None,None

if __name__ == "__main__":
    import csv
    import warnings
    import os
    from tqdm import tqdm 
    
    # Suppress warnings
    warnings.filterwarnings("ignore")
    
    # Define parameters
    reduced_in = False
    Ramona_norm = False
    LS02 = False
    meth = "Steady" #hehe.meth.
    kfold = 5
    seed_list = [42,33,11,5,2]
    feature_engineering = True
    # seed = 42
    # seed = seed_list[0]

    
    counter = 0
    
    # Create a directory for results if it doesn't exist
    some_list = ["pH","Leitfahigkeit","L/S kumuliert","Calcium","Arsenic", "Lead (Pb+2)","Copper","Total Organic C (TOC)",
                 "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"] #Missing: Molybdan



    some_list_2= ["Calcium","Arsenic", "Lead (Pb+2)","Copper","Total Organic C (TOC)",
                 "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"]

    # Generate features and labels
    Experiment_names = ["Nasschlacke 1","Nasschlacke 2","Nasschlacke 3","Nasschlacke 4","Nasschlacke 5",
                            "Nasschlacke 6","Nasschlacke 7","Nasschlacke 8","Nasschlacke 9",
                            "Mischung 1","Mischung 2","Mischung 3","Mischung 4","Mischung 5",
                            "Trockenschlacke 1","Trockenschlacke 2","Trockenschlacke 3","Trockenschlacke 4","Trockenschlacke 5",
                            "Trockenschlacke 6","Trockenschlacke 7","Trockenschlacke 8", 
                            "Magnetische Schlacke"]
    for Ename in Experiment_names:
        First_three = True
        First_two = False    
        # leave_out = Experiment_names[0]
        leave_out = Ename
        Feats, Labs,_,names, Feats_leftout,Labels_leftout = Make_Features_and_Labels(some_list, method="Steady",add_noise=False,std_width = 10,factor=3,add_extra = False,LS02 = False,leave_out=leave_out, First_three=First_three, First_two = First_two )
        Features = Feats
        Labels = Labs
        pick_inputs = None#["Sulfate","L/S kumuliert","Leitfahigkeit", "pH ","Calcium","Chloride","Total Organic C (TOC)"]
        LS_value = None
        results_folder = os.path.join("Pick_Results_first"+str(LS_value), "Random_Forest")
        for elems in some_list_2:
            if elems not in ["Calcium"]:
                continue
            name = 'RF_results_E_' + str(elems) +'_'+str(Ename)+ '.csv'
            counter = 0
            Element = str(elems)
            if not os.path.exists(results_folder):
                os.makedirs(results_folder)
            
            # Define the file path within the results folder
            file_path = os.path.join(results_folder, name)
            
            with open(file_path,'a',newline='') as csvfile:
                writer = csv.writer(csvfile)
                header = ["Reduced input", "Ramona norm", "method", "Kernel", "#K fold","Seed"]
                for elem in some_list_2:
                    if elem == Element:
                    # if True:
                        header.extend([elem + "R2", elem + "wmap", elem + "Relative",elem + "RMSE", elem + "R2 train", elem + "wmap train", elem +"Rel train",elem + "RMSE Train",elem + "R2 Leave",elem + "WMAP Leave", elem + "Relative Leave",elem + "RMSE Leave"])
                writer.writerow(header)
            
                results_leftout_pred = []
                results_leftout_true = []
                results_test_pred = []
                results_test_true = []
                results_validation_pred = []
                results_validation_true = []
            
                for seed in (seed_list):
                    
                    # print("Seed percentage: "+str(100*counter/len(seed_list)))
                    np.random.seed(seed)
                    
            
                        
                    
                    
                    # Open CSV file in append mode
                    n_estimators = 200
                    row_data = [reduced_in, Ramona_norm, meth, n_estimators, kfold, seed]
                    counter = 0
                    # Loop through elements in some_list_3
                    for elem in tqdm(some_list_2, desc = "Elements", position=0, leave=True):
                        
                        # Call GaussianProcess function to get r2, MSE, and kjerne
                        # print(elem)
                        if elem == Element:#0.55
                        # if True:
                        
                            r2, wmap, rel,RMSE, r2_train,wmap_train, Rel_train,RMSE_train,r2leave,wmapleave,relleave,rmseleave,y_validation_true,y_validation_pred,y_test_true,y_test_pred,Leftout_true, Leftout_pred = Random_Forest(Features, Labels,Feats_leftout,Labels_leftout, names, kfold,n_estimators,seed, patience=5, Hold_out=False, std_width=0.1, add_noise=False, factor=2, name=str(elem), reduced_input=reduced_in,LS_value=LS_value,save_plot=True,name_leftout=str(Ename),results_folder=results_folder,pick_inputs=pick_inputs,feature_engineering=feature_engineering)
                            
                            row_data.append(r2)
                            row_data.append(wmap)
                            row_data.append(rel)
                            row_data.append(RMSE)
                            row_data.append(r2_train)
                            row_data.append(wmap_train) 
                            row_data.append(Rel_train)
                            row_data.append(RMSE_train)
                            row_data.append(r2leave)
                            row_data.append(wmapleave)
                            row_data.append(relleave)
                            row_data.append(rmseleave)
                            
                            
                            results_validation_pred.append(y_validation_pred)
                            results_validation_true.append(y_validation_true)
                            results_test_true.append(y_test_true)
                            results_test_pred.append(y_test_pred)
                            results_leftout_true.append(Leftout_true)
                            results_leftout_pred.append(Leftout_pred)
                            # print(elem)
                            # print(r2)
                            print("The data was:")
                            print(r2, wmap, rel,RMSE, r2_train,wmap_train, Rel_train,RMSE_train,r2leave,wmapleave,relleave,rmseleave)
                            # print(MSE)
                        # Append element-specific data to the row
                    # row_data.extend([elem, MSE, r2])
                        
                        # Write row to CSV file
                    writer.writerow(row_data)
                results_validation_pred = np.mean(np.array(results_validation_pred),axis=0).flatten()
                results_validation_true = np.mean(np.array(results_validation_true),axis=0).flatten()
                results_test_true = np.mean(np.array(results_test_true),axis=0).flatten()
                results_test_pred = np.mean(np.array(results_test_pred),axis=0).flatten()
                results_leftout_true = np.mean(np.array(results_leftout_true),axis=0).flatten()
                results_leftout_pred = np.mean(np.array(results_leftout_pred),axis=0).flatten()
                output_file_path = os.path.join(results_folder, "Raw_output_"+str(Ename)+'_'+str(Element)+'LS_'+str(LS_value)+'.csv')
                
                data_dict = {
                    'validation_true': results_validation_true,
                    'validation_pred': results_validation_pred,
                    'test_true': results_test_true,
                    'test_pred': results_test_pred,
                    'leftout_true': results_leftout_true,
                    'leftout_pred': results_leftout_pred
                }
                df = pd.DataFrame(dict([(k, pd.Series(v)) for k, v in data_dict.items()]))
                df.to_csv(output_file_path, index=False)
                print(f"Data saved to {output_file_path}")
        
        
        
    
