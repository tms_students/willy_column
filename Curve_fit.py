import numpy as np
from Last_Preprocess_Overall import Make_Features_and_Labels
import torch
import torch.nn as nn
from sklearn.model_selection import KFold
import torch.optim as optim
from torch.utils.data import DataLoader, TensorDataset
import torch.nn.functional as F
from sklearn.metrics import r2_score
from sklearn.preprocessing import MinMaxScaler,StandardScaler
import difflib
from sklearn.datasets import load_iris #Just to check
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.metrics import mean_squared_error,mean_absolute_percentage_error
# import scipy.optimize.curve_fit as curve_fit
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

some_list = ["pH","Leitfahigkeit","L/S kumuliert","Calcium","Arsenic", "Lead (Pb+2)","Copper",
             "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"] #Missing: Molybdan

some_list_2 = ["Calcium","Arsenic", "Lead (Pb+2)","Copper",
             "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"] 

Features, Labels,_,names = Make_Features_and_Labels(some_list, method="Steady",add_noise=False,std_width = 0.01,factor=4,add_extra = False,Boolnorm = False) #method = Rolling, Steady
#add noise: adds noisy data. Adding noise in "Machine_Learning" will only add the noise when we 


def Curve_fit_(Features, Labels,names,K_folds,std_width = 0.1, add_noise = True, factor = 5,name = "Copper (Cu)", reduced_input = True):
    '''
    Parameters
    ----------
    Features : Features for the model
    Labels : Labels for the model
    K_folds : How many folds using kfolds
    std_width : if add_noise==True: Create new data, std_width=0.01 draws gaussian samples about 
    other values with std_dev 0.01. The default is 0.01.
    add_noise : Boolean. DEfault = True
    factor : Integer. Factor by how much should we increase data? if 3, our dataset will consist of original,
    plus 3 with added noise, making the training data 4 times as big. The default is 3.
    names: List of names in the data, like Calcium, L/S, etc;
    name: The target label. Model will only predict the name of this label.

    Returns
    -------
    x : TYPE
        DESCRIPTION.

    '''
    shape_1 = np.shape(Features) 
    shape_2 = np.shape(Labels)
    print("Fitting linear on: "+str(name)+"...")
    # print(shape_1,shape_2)

    # Define the number of folds
    k_folds = K_folds
    
    # Initialize the KFold object
    kf = KFold(n_splits=k_folds, shuffle = True, random_state=42)
    
    # Initialize lists to store the R-squared scores for each fold
    r2_scores = []
    wmap_score=[]
    Relative_score = []
    
    r2_scores_train = []
    wmap_score_train = []
    Relative_score_train = []
    RMSE_train = []
    RMSE_test = []
    # Convert feature matrix and labels to numpy array
    Feature_Matrix_ = np.array(Features)
    Label_Vector_ = np.array(Labels)
    
    # Initialize MinMaxScaler
    scaler = MinMaxScaler() #C = (c_i - c_min)/(c_max - c_min) -> Range in [0,1]
    thefold = 0
    # Create DataLoader for training and testing
    for train_index, test_index in kf.split(Feature_Matrix_):
        thefold +=1
        X_train_fold, X_test_fold = Feature_Matrix_[train_index], Feature_Matrix_[test_index]
        y_train_fold, y_test_fold = Label_Vector_[train_index], Label_Vector_[test_index]
        
        #Here we should make more data from noise on the train folds. Perhaps this will give us convergence.
        Feat_list = list(X_train_fold)
        Lab_list = list(y_train_fold)
        
        if add_noise: #This adds noisy data to the testing folds, to create more to train on.
            #Here we should make more data from noise on the train folds. Perhaps this will give us convergence.
            Feat_list = list(X_train_fold)
            Lab_list = list(y_train_fold)
            m,n = np.shape(Feat_list)
            noise_Feats = []
            noise_Labs = []
            for times in range(factor):
                for row in Feat_list: #This moves row by row
                    temp_row = []
                    for value in row: #This moves along the row
                        # std_dev = np.abs(value*std_width) #This is how big the fluctuation of the data should be. i.e "how much noise".
                        # print(std_dev)
                        std_dev = std_width
                        noise = np.random.normal(value, std_dev,1)[0] #This creates some noise.
                        temp_row.append(noise)
                    noise_Feats.append(temp_row)
                for row in Lab_list:
                    temp_row = []
                    for value in row:
                        # std_dev = np.abs(value*std_width) #This is how big the fluctuation of the data should be. i.e how much noise.
                        std_dev = std_width
                        noise = np.random.normal(value, std_dev,1)[0] #This creates some noise.
                        temp_row.append(noise)
                    
                    noise_Labs.append(temp_row)
            Feat_list = Feat_list + noise_Feats
            Lab_list = Lab_list + noise_Labs
            
            X_train_fold = np.array(Feat_list)
            y_train_fold = np.array(Lab_list)
        
        if len(name)!=0:
            close_match = difflib.get_close_matches(name, names, n=1,cutoff = 0.6)
            index = (np.where(np.array(names) == close_match)[0][0])-3 #-3 since first three are removed from labels, and names has len(features)
            
            
            y_train_fold = y_train_fold[:,index+3]
            # y_train_fold = y_train_fold.reshape(-1,1)
            y_test_fold = y_test_fold[:,index+3]
            # y_test_fold = y_test_fold.reshape(-1,1)
            shape_2 = np.shape(y_train_fold)
            
            X_values = X_train_fold[:,2]
            y_values =y_train_fold
            
            unique_X = np.unique(X_values)
            
            # Initialize lists to store unique X and averaged y values
            unique_X_values = []
            averaged_y_values = []
            
            # Iterate over unique X values
            for x in unique_X:
                # Find indices where X_values == x
                indices = np.where(X_values == x)[0]
                
                # Extract corresponding y values
                corresponding_y_values = y_values[indices]
                
                # Calculate average of corresponding y values
                avg_y_value = np.mean(corresponding_y_values)
                
                # Append unique X value and averaged y value to lists
                unique_X_values.append(x)
                averaged_y_values.append(avg_y_value)            
            

        # Scale the features and labels
        epsilon = 0.01

        unique_X_values = np.array(unique_X_values)
        averaged_y_values = np.array(averaged_y_values)
        
        # plt.scatter(unique_X_values,averaged_y_values,label = "Uniques train"+str(thefold))
        
        xtestdata = X_test_fold[:,2]
        ytestdata = y_test_fold
        xtraindata = X_train_fold[:,2]
        
        xdata = unique_X_values
        ydata = averaged_y_values

        def wmape(actual, pred):
            actual = np.array(actual)
            pred = np.array(pred)
            wMAPE = np.sum(np.abs(actual-pred))/np.sum(np.abs(actual))*100
            return wMAPE
        
        def Relative_loss(actual,pred):
            norm = len(actual)
            actual = np.array(actual)
            pred = np.array(pred)
            Relative_loss = np.sum(np.abs(actual-pred)/(np.abs(actual)))/norm
            return Relative_loss

        def expo_func(x,a,b,c,d):
            return a*np.exp(-b*x+d) + c


        popt,pcov = curve_fit(expo_func, xdata,ydata)
        
        y_pred = expo_func(xtestdata,*popt)
        y_true = ytestdata
        y_pred_2 = expo_func(unique_X_values,*popt)
        
        X_values = xtestdata
        y_values = ytestdata
        y_pred_values = y_pred
        colors = plt.cm.viridis(np.linspace(0, 1, len(X_values)))
        plt.figure()
        # plt.scatter(xtestdata,ytestdata, label = "Test data")
        plt.scatter(unique_X_values,averaged_y_values,label = "Uniques train"+str(thefold))
        plt.plot(unique_X_values,y_pred_2,label = "Predictions on unique")
        for i, (x_val, y_val) in enumerate(zip(X_values, y_values)):
            plt.scatter(x_val, y_val, label="True values" if i == 0 else "", marker='s', color=colors[i])
        for i, (x_val, y_prd) in enumerate(zip(X_values, y_pred_values)):
            plt.scatter(x_val, y_prd, label = "Predictions"if i == 0 else "", marker='x', color=colors[i])
        plt.title(str(thefold))
        plt.grid(True)
        plt.legend()
        
        plt.figure()
        plt.title("Pred vs Actual")
        for i, (y_val, y_prd) in enumerate(zip(y_values, y_pred_values)):
            plt.scatter(y_val, y_prd, label = "Plot"if i == 0 else "", marker='x', color=colors[i])
        plt.plot([min(y_values), max(y_values)], [min(y_values), max(y_values)], color='black', linestyle='--', linewidth=2)
        plt.ylabel("Predicted values")
        plt.xlabel("Actual values")
        plt.grid(True)
        plt.legend()
        plt.show()
        
        avg_r2_score = np.mean(r2_scores)
    
    
        r2 = r2_score(y_test_fold, y_pred)
        r2_scores.append(r2)
        
        romana_loss = wmape(y_test_fold,y_pred)
        wmap_score.append(romana_loss)
        Relative_score.append(Relative_loss(y_test_fold,y_pred))
        
        y_true_test = y_train_fold 
        y_pred_test = expo_func(xtraindata,*popt)

        
        r2_test = r2_score(y_true_test,y_pred_test)
        r2_scores_train.append(r2_test)
        wmap_score_train.append(wmape(y_true_test,y_pred_test))
        Relative_score_train.append(Relative_loss(y_true_test,y_pred_test))
        from sklearn.metrics import mean_squared_error
        RMSE_train.append(mean_squared_error(y_true_test,y_pred_test))
        RMSE_test.append(mean_squared_error(y_test_fold,y_pred))
        # print(f"Fold R-squared: {r2:.4f}")
    plt.legend()
    plt.show()
    # Calculate the average R-squared score across all folds
    avg_r2_score = np.mean(r2_scores)
    # print("Average R-squared score:", avg_r2_score)
    
    return avg_r2_score,np.mean(wmap_score),np.mean(Relative_score),np.mean(RMSE_test), np.mean(r2_scores_train), np.mean(wmap_score_train), np.mean(Relative_score_train),np.mean(RMSE_train)
    

if __name__ == "__main__":
    import csv
    import warnings
    import os
    from tqdm import tqdm 
    # Suppress warnings
    warnings.filterwarnings("ignore")
    
    # Define parameters
    reduced_in = False
    Ramona_norm = False
    LS02 = True
    meth = "Steady" #hehe.meth.
    kfold = 5
    seed_list = [42,33,11,5,2]
    # seed = 42
    # seed = seed_list[0]
    name = 'Curve_Fit_results_Chloride.csv'
    Element = 'Chloride'
    counter = 0
    # Create a directory for results if it doesn't exist
    results_folder = "Results_Linear"
    results_folder = os.path.join("A_Results", "Curve_fit")
    if not os.path.exists(results_folder):
        os.makedirs(results_folder)
    
    # Define the file path within the results folder
    file_path = os.path.join(results_folder, name)
    
    with open(file_path,'a',newline='') as csvfile:
        writer = csv.writer(csvfile)
        header = ["Reduced input", "Ramona norm", "method", "Alpha", "K fold","Seed"]
        for elem in some_list_2:
            if elem == Element:
                header.extend([elem + "R2", elem + "wmap", elem + "Relative",elem + "RMSE", elem + "R2 train", elem + "wmap train", elem +"Rel train",elem + "RMSE Train"])
        writer.writerow(header)
    
    
        for seed in seed_list:
            
            print("Seed percentage: "+str(100*counter/len(seed_list)))
            counter+=1
            np.random.seed(seed)
            
    
                
            # Generate features and labels
            Features, Labels, _, names = Make_Features_and_Labels(some_list, method=meth, add_noise=False, std_width=0.01, factor=4, add_extra=False, Boolnorm=Ramona_norm,LS02=LS02)
            
            # Open CSV file in append mode
            
                
            # kjerne = 1.0 * RBF(length_scale=1.0, length_scale_bounds=(1e-2, 1e3)) + WhiteKernel(noise_level=1e-5, noise_level_bounds=(1e-10, 1e+1)) + DotProduct()
            Alpha = 1.0
            row_data = [reduced_in, Ramona_norm, meth, Alpha, kfold, seed]
            # Loop through elements in some_list_3
            for elem in tqdm(some_list_2, desc = "Elements", position=0, leave=True):
                # Call GaussianProcess function to get r2, MSE, and kjerne
                # print(elem)
                if elem == Element:
                    r2, wmap, rel,RMSE, r2_train,wmap_train, Rel_train,RMSE_train= Curve_fit_(Features, Labels,names, K_folds=5,std_width = 0.1, add_noise = False, factor = 2, name=str(elem),reduced_input=reduced_in)
                    
                    row_data.append(r2)
                    row_data.append(wmap)
                    row_data.append(rel)
                    row_data.append(RMSE)
                    row_data.append(r2_train)
                    row_data.append(wmap_train) 
                    row_data.append(Rel_train)
                    row_data.append(RMSE_train)
                    print(r2, wmap, rel, r2_train,wmap_train, Rel_train)
                # Append element-specific data to the row
            # row_data.extend([elem, MSE, r2])
                
                # Write row to CSV file
            writer.writerow(row_data)
        
        
        
        
        
        



