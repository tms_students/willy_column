import numpy as np
import pandas as pd
import os
from difflib import SequenceMatcher
import matplotlib.pyplot as plt
from Load_Neural_And_Evaluate import Plotting_and_errors
from sklearn.metrics import r2_score, mean_squared_error
from Last_Preprocess_Overall import Make_Features_and_Labels
import joblib
import difflib
from sklearn.preprocessing import MinMaxScaler,StandardScaler,RobustScaler
import shap
import re

def Relative_loss(actual, pred):
    norm = len(actual)
    actual = np.array(actual)
    pred = np.array(pred)
    
    # Exclude zeros from actual values
    non_zero_mask = actual != 0
    print(non_zero_mask)
    actual_non_zero = actual[non_zero_mask]
    pred_non_zero = pred[non_zero_mask]
    
    # Calculate relative loss
    relative_loss = np.sum(np.abs(actual_non_zero - pred_non_zero) / np.abs(actual_non_zero)) / norm
    return relative_loss

def Plot_errorbars(actual,predicted,Element):
    arr_a = actual
    arr_p = predicted
    medians_a = np.median(arr_a, axis=0)
    std_devs_a = np.std(arr_a, axis=0)
    
    Number_of_Experiments, Number_of_LS_vals = np.shape(actual)
    medians_p = np.median(arr_p, axis=0)
    std_devs_p = np.std(arr_p, axis=0)
    
    LSNames = ['1','2','5','10']
    LSNames = LSNames[:Number_of_LS_vals]
    
    # plt.subplot(1, 2, 1)
    # plt.boxplot(arr_a, labels=[x for x in LSNames])
    # plt.xlabel('Columns')
    # plt.ylabel('Values')
    # plt.title('Box Plot of arr_a')
    # plt.grid(True)
    
    # plt.subplot(1, 2, 2)
    # plt.boxplot(arr_p, labels=[x for x in LSNames])
    # plt.xlabel('Columns')
    # plt.ylabel('Values')
    # plt.title('Box Plot of arr_p')
    # plt.grid(True)
    
    # plt.tight_layout()
    # plt.show()
    
    # Plot the median and standard deviation
    x = np.arange(1, arr_a.shape[1] + 1)
    
    plt.figure(figsize=(12, 6))
    
    plt.errorbar(x, medians_a, yerr=std_devs_a, fmt='o', label='Actual')
    plt.errorbar(x, medians_p, yerr=std_devs_p, fmt='o', label='Predicted')
    
    plt.xticks(x, [x for x in LSNames])
    plt.xlabel('LS Values')
    plt.ylabel('Concentrations')
    plt.title('Median and Standard Deviation of '+str(Element))
    plt.legend()
    plt.grid(True)
    plt.tight_layout()
    plt.show()
    return None


def extract_columns_until_nan(arr):
    """
    Extract columns of an array up to the first column that contains only NaN values.
    
    Parameters:
    arr (np.ndarray): The input array with shape (m, n).
    
    Returns:
    np.ndarray: The resulting array with shape (m, k) where k is the number of valid columns.
    """
    # Ensure the input is a numpy array
    arr = np.array(arr)
    
    # Check if the array has at least two dimensions
    if len(arr.shape) < 2:
        raise ValueError("Input array must have at least two dimensions.")
    
    # Find the index of the first column that contains only NaN values
    first_nan_col_index = np.where(np.isnan(arr).all(axis=0))[0]
    
    # Determine the slice up to the first all-NaN column
    if first_nan_col_index.size > 0:
        valid_columns = arr[:, :first_nan_col_index[0]]
    else:
        valid_columns = arr  # If no all-NaN columns, take all columns
    
    return valid_columns
def Read_and_Eval(path,Element,Experiment,Features,Labels,Feats_leftout, Labs_leftout,feature_names,model_name="Lasso",ColName=None,All = True,LS_value=None,reduced_input=False,pick_inputs=None,feature_engineering=False):
    #This function will just calculate the values stored in the excel files.
    Label_Vector_ = np.array(Labels)
    Label_Vector_leftout = np.array(Labs_leftout)
    Feature_Matrix_= np.array(Features)
    Feature_Matrix_leftout = np.array(Feats_leftout)
    
    name=Element

    
    if LS_value is not None: #Remove the training for small LS values.
        if isinstance(LS_value, list):
            mask = ~np.isin(Label_Vector_[:,2], LS_value)  # Inverse the condition
            Leftmask = ~np.isin(Label_Vector_leftout[:,2], LS_value)
        else:
            mask = (Label_Vector_[:,2] == LS_value)  # Inverse the condition
            Leftmask = (Label_Vector_leftout[:,2] == LS_value) 

        # mask = (Label_Vector_[:,2] != LS_value)
        Feature_Matrix_ = Feature_Matrix_[mask]
        Label_Vector_ = Label_Vector_[mask]
        
        Label_Vector_leftout = Label_Vector_leftout[Leftmask]
        Feature_Matrix_leftout = Feature_Matrix_leftout[Leftmask]
    
    
    Feats=Feature_Matrix_
    if len(name)!=0:
        close_match = difflib.get_close_matches(name + "LS01", names, n=1,cutoff = 0.6)
        if len(close_match) == 0:
            close_match = difflib.get_close_matches(name, names, n=1,cutoff = 0.6)#-3 since first three are removed from labels, and names has len(features)
        index = (np.where(np.array(names) == close_match)[0][0])-3 #-3 since first three are removed from labels, and names has len(features)
    
        
        # Feats = Feature_Matrix_[:,index+3]
        # Feats = Feats.reshape(-1,1)
        
        Labs = Label_Vector_[:,index+3]
        Labs = Labs.reshape(-1,1)

        Label_Vector_leftout = Label_Vector_leftout[:,index+3]
        Label_Vector_leftout = Label_Vector_leftout.reshape(-1,1)
    
        shape_2 = np.shape(Labs)
        
        
        if reduced_input: #This will make only LS, Leitf, LS*elem, plus element of output to input.
            # X_train_fold = np.concatenate((X_train_fold[:, :3], X_train_fold[:, index+3].reshape(-1, 1)), axis=1)
            Feats = np.concatenate((Feats[:, :3], Feats[:, index+3].reshape(-1, 1), (Feats[:, 2] * Feats[:, index+3]).reshape(-1, 1)), axis=1)
            # X_train_fold = X_train_fold.reshape(-1,1)
            Feature_Matrix_leftout = np.concatenate((Feature_Matrix_leftout[:, :3], Feature_Matrix_leftout[:, index+3].reshape(-1, 1), (Feature_Matrix_leftout[:, 2] * Feature_Matrix_leftout[:, index+3]).reshape(-1, 1)), axis=1)
            shape_1 = np.shape(Feats)
            

            string_to_add = "LS*"+str(name)
            string_series = pd.Series([string_to_add])
            a_string_series = pd.Series([str(name)])
            namese = pd.concat([names[:3], a_string_series,string_series])
            namese=namese.reset_index(drop=True)
        else: 
            if feature_engineering:
                Feats = np.concatenate((Feats, (Feats[:, 2] * Feats[:, index+3]).reshape(-1, 1).reshape(-1, 1),(1/Feats[:, 2] * Feats[:, index+3]).reshape(-1, 1),(np.exp(-Feats[:, 2]) * Feats[:, index+3]).reshape(-1, 1)), axis=1)
                
                Feature_Matrix_leftout = np.concatenate((Feature_Matrix_leftout, (Feature_Matrix_leftout[:, 2] * Feature_Matrix_leftout[:, index+3]).reshape(-1, 1).reshape(-1, 1),(1/Feature_Matrix_leftout[:, 2] * Feature_Matrix_leftout[:, index+3]).reshape(-1, 1),(np.exp(-Feature_Matrix_leftout[:, 2]) * Feature_Matrix_leftout[:, index+3]).reshape(-1, 1)), axis=1)
                string_to_add = "LS*"+str(name)
                string_to_add_2 = "LS/"+str(name)
                string_to_add_3 = "exp(-"+str(name) + ')'
                string_series = pd.Series([string_to_add,string_to_add_2,string_to_add_3])
                namese = pd.concat([names, string_series])
                namese=namese.reset_index(drop=True)
            if feature_engineering == False:
                Feats = np.concatenate((Feats, (Feats[:, 2] * Feats[:, index+3]).reshape(-1, 1).reshape(-1, 1)), axis=1)
                Feature_Matrix_leftout = np.concatenate((Feature_Matrix_leftout, (Feature_Matrix_leftout[:, 2] * Feature_Matrix_leftout[:, index+3]).reshape(-1, 1).reshape(-1, 1)), axis=1)
                shape_1 = np.shape(Feats)
                
                    
                string_to_add = "LS*"+str(name)
                string_series = pd.Series([string_to_add])
                namese = pd.concat([names, string_series])
                namese=namese.reset_index(drop=True)
        nameinput = name
        if pick_inputs is not None:
            def get_closest_indices(names, names_to_keep):
                closest_indices = []
                for name in names_to_keep:
                    
                    closest_match1 = difflib.get_close_matches(name, names, n=1, cutoff=1) #Exact if L/S Kumuliert, LS*Calcium, pH , etc;
                    if closest_match1:
                        closest_indices.append(np.where(names == closest_match1[0])[0][0])
                    else:
                        # Find the closest base name in the names array (ignoring LSX)
                        closest_match = difflib.get_close_matches(name, names, n=3, cutoff=0.5)
                        if name == nameinput:
                            closest_match = difflib.get_close_matches(name, names, n=4, cutoff=0.5) #Because also LS*element exists. So there are 4.
                        # print("Name:", name)
                            if feature_engineering:
                                closest_match = difflib.get_close_matches(name, names, n=6, cutoff=0.5) 
                        # print("Base Names:", base_names)
                        
                        if closest_match:
                            for matches in closest_match:
                                
                                closest_indices.append(np.where(names == matches)[0][0])
                return closest_indices, pick_inputs
            
            
            indices_to_use, _ = get_closest_indices(namese, pick_inputs)

            namese = namese[indices_to_use].reset_index(drop=True)

            Feats=Feats[:,indices_to_use]
            Feature_Matrix_leftout = Feature_Matrix_leftout[:,indices_to_use]

    scaler = StandardScaler()
    # scaler = RobustScaler()
    # scaler = MinMaxScaler()
    
    #StandardScaler,RobustScaler
    combined_features = np.vstack((Feats, Feature_Matrix_leftout))
    scaled_combined_features = scaler.fit_transform(combined_features)
    Feats = scaled_combined_features[:Feats.shape[0]]
    Feature_Matrix_leftout = scaled_combined_features[Feats.shape[0]:]
    
    Labels = Labs
    Labs_leftout = Label_Vector_leftout
    Features = Feats
    
    Feats_leftout = (Feature_Matrix_leftout)
    

    raw_output=[]
    files = os.listdir(path)
    csv_files = [file for file in files if file.endswith('.csv')]
    for csv_file in csv_files:
        if Element in csv_file and Experiment in csv_file and "Raw_output" not in csv_file:
            csv_path = os.path.join(path, csv_file)
            df = pd.read_csv(csv_path)
    for csv_file in csv_files:
        if All:
            
            if Element in csv_file and Experiment in csv_file and "Raw_output" in csv_file:
                csv_path = os.path.join(path, csv_file)
                raw_output = pd.read_csv(csv_path)
        elif All == False:
            if Element in csv_file and Experiment in csv_file and "Raw_output" in csv_file:
                if any(key in csv_file for key in ["LS_1", "LS_2", "LS_5", "LS_10"]):
                    csv_path = os.path.join(path, csv_file)
                    dftemp = pd.read_csv(csv_path)
                    raw_output.append(dftemp)
    if len(np.shape(raw_output)) == 2:
        m,l =  np.shape(raw_output)
    elif len(np.shape(raw_output)) == 3:
        m,l,p = np.shape(raw_output)
    df_extra = []
    if All == True:
        df_tempo = pd.DataFrame(raw_output)
        df_extra.append(df_tempo)
    if All == False: 
        for i in range(m):
            df_tempo = pd.DataFrame(raw_output[i])
            df_extra.append(df_tempo)
        raw_output = pd.concat(df_extra, axis=1)
    # df = pd.read_csv(csv_path)
    Column_name = str(Element)+'Rel Leave'
    if ColName is not None:
        Column_name = ColName
    
    sanitized_Element = re.escape(Element)

    
    path_model = os.path.join(path, str(model_name)+'_model'+str(Experiment)+'_'+str(Element))
    model_saves = os.listdir(path)
    pattern = re.compile(rf"{model_name}_model{Experiment}_{sanitized_Element}_\d+")
    test_preds = []
    train_preds = []

    
    
    X_test=Feats_leftout
    X_train = Feats
    y_train = Labels.flatten()
    y_test = Labs_leftout.flatten()
    
    counter=0
    for save in model_saves:
        if pattern.match(save):
            counter+=1
            model_path = os.path.join(path, save)
            model = joblib.load(model_path)
            
            test_pred = model.predict(X_test)
            train_pred = model.predict(X_train)
            
            test_preds.append(test_pred)
            train_preds.append(train_pred)
    print("We found: ",counter," models")
    avg_test_pred = np.mean(test_preds, axis=0)
    avg_train_pred = np.mean(train_preds, axis=0)

    
    
    
    test_pred = np.abs(avg_test_pred) #Somehow e.g. GPR can yield negatives. Which is always wrong, of course.
    train_pred = np.abs(avg_train_pred)
    Ename = Experiment
    feature_names = namese
    plot_shap(X_test,X_train,y_train,y_test,model,Ename,feature_names,save_plot=False)
    gactual = ''
    gpredicted=''
    LS_train = X_train[:,2]
    gLS=''
    

    actual_l = y_test
    pred_l = test_pred



    #actual, predicted, gactual,gpredicted,LS, gLS,predleftout = None, Lleftout=None, name_of_model="",element = "",Ename=None,add_noise=False,Number_of_experiemnts=None): #Actual, predicted, generated actuals and generated predicted.
    Plotting_and_errors(y_train,train_pred,gactual,gpredicted,LS_train,gLS,test_pred,y_test,name_of_model=str(model_name),element=name,Ename = Ename,add_noise=add_noise)
    # print(csv_files)
    
    return df, raw_output,actual_l,pred_l

def plot_shap(X_test,X_train,y_train,y_test,model,Ename,feature_names,save_plot=False):
    K = 10
    def predict_wrapper(model, X):
        return model.predict(X).flatten() #To deal with some weird output array...
    # background_data_summary = shap.kmeans(X_train, K)
    background_data_summary = shap.sample(X_train, 100)

    # explainer = shap.KernelExplainer(model.predict, background_data_summary)
    explainer = shap.KernelExplainer(lambda x: predict_wrapper(model, x), background_data_summary)
    # explainer = shap.KernelExplainer(lambda x: predict_wrapper(model, x), X_train_fold)
    shap_values = explainer.shap_values(X_test)
    shap.summary_plot(shap_values,X_test, feature_names = feature_names,show=False)
    test_preds = model.predict(X_test)
    train_preds = model.predict(X_train)
    r2_train = r2_score(y_train,train_preds)
    r2 = r2_train

    plt.title(r'$\bf{Lasso}$' + " \nElement: {} , R2 value: {:.2f}".format(name, r2))
    if save_plot:
        Ename
        plt.savefig(os.path.join(results_folder, f"shap_summary_plot_Lasso_{name}_{Ename}.png"))
    plt.show()

if __name__ == "__main__":
    some_list = ["pH","Leitfahigkeit","L/S kumuliert","Calcium","Arsenic", "Lead (Pb+2)","Copper","Total Organic C (TOC)",
                 "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"] #Missing: Molybdan
    
    some_list_2 = ["Calcium","Arsenic", "Lead (Pb+2)","Copper","Total Organic C (TOC)",
                 "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"] 
    
    add_noise=False
    save = False
    LS_Value = None
    reduced_in = False
    Ramona_norm = False
    LS02 = False
    feature_engineering=True
    factor = 5
    pick_inputs = None#["Sulfate","L/S kumuliert","Leitfahigkeit", "pH ","Calcium","Chloride","Total Organic C (TOC)"]#["Sulfate","L/S kumuliert","Leitfahigkeit", "pH ","Calcium","Chloride","Total Organic C (TOC)"]#["Sulfate","L/S kumuliert","Leitfahigkeit", "pH ","Calcium","Total Organic C (TOC)"]
    
    meth = "Steady" #hehe.meth.
    
    Experiment_names = ["Nasschlacke 1","Nasschlacke 2","Nasschlacke 3","Nasschlacke 4","Nasschlacke 5",
                            "Nasschlacke 6","Nasschlacke 7","Nasschlacke 8","Nasschlacke 9",
                            "Mischung 1","Mischung 2","Mischung 3","Mischung 4","Mischung 5",
                            "Trockenschlacke 1","Trockenschlacke 2","Trockenschlacke 3","Trockenschlacke 4","Trockenschlacke 5",
                            "Trockenschlacke 6","Trockenschlacke 7","Trockenschlacke 8", 
                            "Magnetische Schlacke"]
    

    Temp_name = 'H_Results'
    # Temp_name = "Pick_Results_test"+str(LS_Value)
    Temp_name = "Pick_Results_1"
    Temp_name = "Pick_Results_2_standard_feat"+str(LS_Value)
    Temp_name = "Pick_Results_test_feat"+str(LS_Value) #GPR, and Lasso
    Temp_name = "Pick_Results_first"+str(LS_Value) #Ridge
    model_name = 'Gaussian_Process_Regressor'
    name = model_name  # 'Ridge', 'Lasso', 'Random_Forest', 'Gaussian_Process_Regressor'
    base_path = r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column'
    
    path_ridge = f'{base_path}\\{Temp_name}\\Ridge'
    path_lasso = f'{base_path}\\{Temp_name}\\Lasso'
    path_RF = f'{base_path}\\{Temp_name}\\Random_Forest'
    path_GPR = f'{base_path}\\{Temp_name}\\Gaussian_Process_Regressor'
    path_if_statement = f'{base_path}\\{Temp_name}\\Random_Forest'
    
    
    save= False
    All = True
    Element = "Calcium"#"Total Organic C (TOC)"#"Calcium"
    Experiment = Experiment_names[0]
    Column_name_rels = str(Element)+'Rel Leave'
    model_paths = {
    'Ridge': path_ridge,
    'Lasso': path_lasso,
    'Random_Forest': path_RF,
    'Gaussian_Process_Regressor': path_GPR
    }

    # Set the path based on the model_name
    path = model_paths.get(model_name, path_ridge)  # Default to path_ridge if model_name is not found

    
    
    if str(path)== str(path_if_statement):
        print("Was trye")
        Column_name_rels = str(Element)+'Relative Leave'
    Ltruelist = []
    Lpredlist = []
    Names_for_plot = []
    results_relative_for_print = []
    actual_ls = []
    predicted_ls = []
    for Ename in Experiment_names:
        Features, Labels,_,names,Feats_leftout,Labels_leftout = Make_Features_and_Labels(some_list, method="Steady",add_noise=add_noise,std_width = 0.1,factor=factor,add_extra = False,Boolnorm = Ramona_norm,LS02= LS02, leave_out =Ename, First_three=True) #method = Rolling, Steady'
        
        if Ename in ["Nasschlacke 1","Nasschlacke 2","Nasschlacke 3","Nasschlacke 4","Nasschlacke 5",
                                "Nasschlacke 6","Nasschlacke 7","Nasschlacke 8","Nasschlacke 9",
                                "Mischung 1","Mischung 2","Mischung 3","Mischung 4","Mischung 5",
                                "Trockenschlacke 1","Trockenschlacke 2","Trockenschlacke 3","Trockenschlacke 4","Trockenschlacke 5",
                                "Trockenschlacke 6","Trockenschlacke 7","Trockenschlacke 8", 
                                "Magnetische Schlacke"]:
            datas,raw,Ltrue,Lpred = Read_and_Eval(path,Element,Ename,Features,Labels,Feats_leftout,Labels_leftout,names,ColName=Column_name_rels,All = All,LS_value=LS_Value,model_name = model_name,pick_inputs=pick_inputs,feature_engineering=feature_engineering)

            # Ltrue,Lpred = raw['leftout_true'],raw['leftout_pred']
            Ltruelist.append(Ltrue.flatten())
            Lpredlist.append(Lpred.flatten())

            Number_of_LS = len(Ltrue)

            Names_for_plot.append(Ename)
            Column_name = Column_name_rels
            results_relative_for_print.append(datas[Column_name].values)
    

    
    Ltruelist = np.array(Ltruelist).flatten()
    Lpredlist = np.array(Lpredlist).flatten()
    
    Number_of_Experiments = int(len(Ltruelist)/Number_of_LS)
    Ltruelist=Ltruelist.reshape(Number_of_Experiments,Number_of_LS)
    Lpredlist=Lpredlist.reshape(Number_of_Experiments,Number_of_LS)

    LS_Names_mean = ['1','2','5','10']
    Relative_for_plot_LS = np.zeros(Number_of_LS)
    for i in range(Number_of_LS):
        Relative_for_plot_LS[i] = Relative_loss(Ltruelist[:,i], Lpredlist[:,i])
    LS_Names_mean = LS_Names_mean[:(Number_of_LS)]
    print(LS_Names_mean, Relative_for_plot_LS)
    mean_for_plot = np.mean(Relative_for_plot_LS)
    print("Relative for plot LS:",Relative_for_plot_LS)
    plt.figure()
    plt.title("Using "+str(model_name) + "\nMean Relative Error "+str(Element)+": "+ str(np.round(mean_for_plot,2)))
    plt.scatter(LS_Names_mean,Relative_for_plot_LS, label = "Names and Relative Errors"+name)
    plt.xlabel('LS-Values')
    plt.ylabel("Relative Error")
    plt.grid()
    plt.legend()
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()
    results_relative_for_print = np.array(results_relative_for_print)
    results_folder = r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Results'
    if save:
        plt.savefig(os.path.join(results_folder, str(Element),name))
    plt.show()
    

    Relative_for_plot_experiments = np.zeros(Number_of_Experiments)
    for i in range(Number_of_Experiments):
        Relative_for_plot_experiments[i] = Relative_loss(Ltruelist[i,:], Lpredlist[i,:])
        
    plt.figure()
    plt.title("Using "+str(model_name) +"\nMean Relative Non-Zero Error "+str(Element)+": "+ str(np.round(mean_for_plot,2)))
    plt.scatter(Names_for_plot,Relative_for_plot_experiments, label = "Names and Relative Errors"+name)
    plt.xlabel('Experiment Names')
    plt.ylabel("Relative Error")
    plt.grid()
    plt.legend()
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()
    results_relative_for_print = np.array(results_relative_for_print)
    results_folder = r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Results'
    if save:
        plt.savefig(os.path.join(results_folder, str(Element),name))
    
    

    plt.figure()
    plt.title("Plot by experiment, mean for "+str(Element)+": "+ str(np.round(mean_for_plot,2)))
    for index,name in enumerate(Names_for_plot):
        plt.scatter(Ltruelist[index,:], Lpredlist[index,:], label = name)
    plt.grid(True)
    plt.xlabel("True")
    plt.ylabel("Predicted")
    min_val = min(np.min(Ltruelist), np.min(Lpredlist))
    max_val = max(np.max(Ltruelist), np.max(Lpredlist))
    plt.plot([min_val, max_val], [min_val, max_val], 'k--', lw=2, label='y = x')
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=3)
    plt.show()

    print("R2 Scores:")
    #Lpredlist = [Numberofexp, NumberofLS]
    for i in range(Number_of_LS):
        if Number_of_LS == 1:
            LS_values = LS_Value
            print("LS Value is: ",LS_values)
            print(r2_score(Ltruelist,Lpredlist))
    
    for i in range(Number_of_LS):
        if Number_of_LS != 1:
            LS_values = LS_Value
            print("LS Value is (LS): ",LS_Names_mean[i])
            print(r2_score(Ltruelist[:,i],Lpredlist[:,i]))
    
    print("Total R2 Score:")
    print(r2_score(Ltruelist.flatten(),Lpredlist.flatten()))
    print("NRMSE VALUE:",mean_squared_error(Ltruelist.flatten(),Lpredlist.flatten()) )
    Temp_factor = Number_of_LS
    arr_a_star,arr_p_star = Ltruelist,Lpredlist
    Plot_errorbars(arr_a_star,arr_p_star,str(Element))
    arr_a = arr_a_star.flatten()
    
    Experiment_names_without_ename = Experiment_names[:Number_of_Experiments]
    colors = plt.cm.viridis(np.linspace(0, 1, Number_of_LS))
    colors = ['black','blue','orange','blue','green','grey'][:Number_of_LS]
    plt.figure()
    
    actual = arr_a_star
    predicted = arr_p_star
    LS_names = ['1','2','5','10']
    plt.title(str(model_name))
    for i in range(Number_of_LS):
        
        plt.scatter(actual[:,i], predicted[:,i], marker='x', color=colors[i], label="LS: "+str(LS_names[i]))
    
    plt.plot([min(arr_a), max(arr_a)], [min(arr_a), max(arr_a)], color='black', linestyle='--', linewidth=2)
        # plt.scatter(gactual,gpredicted,marker='x', color='blue', label='Real data')
    plt.ylabel("Predicted values")
    plt.xlabel("Actual values")
    plt.grid(True)
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=3)
    plt.show()
                # plt.figure()
                # plt.scatter(Names_for_plot,results_relative_for_print, label = "Names and Relative Errors")
                # plt.xlabel('Experiment Names')
                # plt.ylabel("Relative Error")
                # plt.grid()
                # plt.legend()
                # plt.xticks(rotation=45, ha='right')
                # plt.tight_layout()
                # results_relative_for_print = np.array(results_relative_for_print)
                # mean_for_plot = np.mean(results_relative_for_print[results_relative_for_print != 0])
                # plt.title("Mean Relative Non-Zero Error "+str(Element)+": "+ str(np.round(mean_for_plot,2)))
                # plt.show()
                # results_folder = r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\ResultsThinLayer'
                # plt.savefig(os.path.join(results_folder, str(Element)))
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    