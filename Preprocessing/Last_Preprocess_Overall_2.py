
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import difflib

from Read_Overall import Get_data
from Preprocessing_Overall import Fill_data
from Pick_Overall import Pick_data
import seaborn as sns



###GETTING DATA DONE#######

'''
For the input we want the following (Thanks Romana for the suggestion):
Always use elements at LS0.1. as one part of the input, the other part is:
LSKumuliert, pH and mobility(Leitfahigkeit) at LS(n-1) 

To find: elemnts' concentration at LS(n)
'''


#Rolling: Less "independent" data, guesses 0.2 from 0.1, then 0.5 from 0.2 and so on. Presumably better if the model can learn fast enough
#Steady: More independent data. But it must guess farther into the future based on a single point.
def Make_Features_and_Labels(some_list,add_noise = True,factor = 2,std_width = 0.1,add_extra= False,Boolnorm = False,LS02 = False,save_datas = False, leave_out = None):
    '''
    Parameters
    ----------
    some_list : List of data to consider; always use pH, Leitfahigkeit and L/S kumuliert!
    method : String, optional:
        How should the data be processed. Rolling: Compare LS(n-1) with LS(n). Steady: LS(0.1) with LS(n). The default is "Rolling".
        
    add_noise: If true, you make a bigger dataset by a factor of "factor". 
    std_width: For each data draw from gauss dist about data with std_width as std_wdith*data. High std_width means more spread. 0.1 means 10% spread about each value.
    add_extra: Since there seems to be strong correlation between LS and the values, one can use add_extra=True 
    to add "LS*concentration" as another row for extra data.
    leave_out: if name of experiment, then this curve is left out.
    
    Returns
    -------
    Features,Labels : (m,n) = (Data points, Length of list): List of Each row is a measurement, each collumn a variable
    Feats_simple: Like features without noise or anythign like that.
    '''
    LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data = Get_data("")
    LSKum001 = Fill_data(LSKum001);LSKum002 = Fill_data(LSKum002);LSKum005 = Fill_data(LSKum005);LSKum010 = Fill_data(LSKum010);
    LSKum050 = Fill_data(LSKum050);LSKum100 = Fill_data(LSKum100);LSKum020 = Fill_data(LSKum020)

    
    
    LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data= Pick_data(LSKum001, LSKum002, LSKum005, LSKum010, LSKum020, LSKum050, LSKum100, names_selected_data, some_list, Normalize = Boolnorm)
    if save_datas:
        for index,LS in enumerate([LSKum001,LSKum002,LSKum005,LSKum010,LSKum020,LSKum050,LSKum100]):
            LSK_df = pd.DataFrame(LS)
            LSK_df.to_csv(str(index) + ".csv", index=False)
    
    # LSKum001= LSKum001.iloc[:,:-4];LSKum002= LSKum002.iloc[:,:-4];LSKum005= LSKum005.iloc[:,:-4];LSKum010= LSKum010.iloc[:,:-4];LSKum020= LSKum020.iloc[:,:-4];LSKum050= LSKum050.iloc[:,:-4];LSKum100= LSKum100.iloc[:,:-4];
    m,n = np.shape(LSKum001)
    Features = []
    Labels = []
    
    for j in range(n): #The order here matters!
        for index,LS in enumerate([LSKum001,LSKum002,LSKum005,LSKum010,LSKum020,LSKum050,LSKum100]):
    
                # if index == 0: #Here we would be comparing first to first.
                #     continue
                if index + 1 < len([LSKum001, LSKum002, LSKum005, LSKum010, LSKum020, LSKum050, LSKum100]):
                    next_LS = [LSKum001, LSKum002, LSKum005, LSKum010, LSKum020, LSKum050, LSKum100][index + 1]
                
                    # temp_labels_upper = next_LS.iloc[0:3, j].values #We dont need to predict LS,pH and leitfahigkeit
                    temp_labels_elements = next_LS.iloc[:, j].values
                    
                    temporary_labels = [];temporary_labels.extend(temp_labels_elements)
                    # temporary_labels = [];temporary_labels.extend(temp_labels_upper);temporary_labels.extend(temp_labels_elements)
                    Labels.append(temporary_labels)
                    
                    temp_upper = next_LS.iloc[2,j] #LS values
                    # print(temp_upper)
                elif index + 1 == len([LSKum001, LSKum002, LSKum005, LSKum010, LSKum020, LSKum050, LSKum100]):
                    # print("Equal index")
                    continue
            #Steady means we only pick the elements from 0.1, to guess all the others
                # temp_labels_elements = LS.iloc[3:,j].values
                # temporary_labels = [];temporary_labels.extend(temp_labels_elements)
                # Labels.append(temporary_labels)
                temporary_features = []
                temp_eh = LSKum001.iloc[0,j]
                temp_ph = LSKum001.iloc[1,j]
                
                temp_eh2 = LSKum002.iloc[0,j]
                temp_ph2 = LSKum002.iloc[1,j]
                
                temp_elements = LSKum001.iloc[3:,j].values #Big difference is always LSKum001!
                temp_elements2 = LSKum002.iloc[3:,j].values
                temporary_features = [temp_eh,temp_ph,temp_upper] + temp_elements.tolist()# + [temp_eh2,temp_ph2] + temp_elements2.tolist()
                #temporary_features = [];temporary_features.extend(temp_eh);temporary_features.extend(temp_ph); temporary_features.extend(temp_upper);temporary_features.extend(temp_elements)
                Features.append(temporary_features)

    Feats_simple = Features
    
    if leave_out is not None: #We now put one experiment on the outside.
        Experiment_names = ["Nasschlacke 1","Nasschlacke 2","Nasschlacke 3","Nasschlacke 4","Nasschlacke 5",
                            "Nasschlacke 6","Nasschlacke 7","Nasschlacke 8","Nasschlacke 9",
                            "Mischung 1","Mischung 2","Mischung 3","Mischung 4","Mischung 5",
                            "Trockenschlacke 1","Trockenschlacke 2","Trockenschlacke 3","Trockenschlacke 4","Trockenschlacke 5",
                            "Trockenschlacke 6","Trockenschlacke 7","Trockenschlacke 8", 
                            "Magnetische Schlacke"]
        closest_match = difflib.get_close_matches(leave_out, Experiment_names, n=1)
        if closest_match:
            Index_experiment = Experiment_names.index(closest_match[0])
            start_index = int(6*Index_experiment)
            end_index = int(6*(Index_experiment+1))
            
            test_features = Features[start_index:end_index]
            test_labels = Labels[start_index:end_index]
            Features = Features[:start_index]+ Features[end_index:]
            Labels = Labels[:start_index]+ Labels[end_index:]
        else:
            print("No experiment with that name was found")
    # closest_match = difflib.get_close_matches(Experiment, Experiment_names, n=1)

    # if closest_match:
    #     Index_experiment = Experiment_names.index(closest_match[0])
    # else:
    #     print("No close match found for:", Experiment)
    #     Index_experiment = None

    if add_noise:
        m,n = np.shape(Features)
        noise_Feats = []
        noise_Labs = []
        for times in range(factor):
            for i,row in enumerate(Features): #This moves row by row
                temp_row = []
                
                for j,value in enumerate(row): #This moves along the row. Only add for non LS.
                    if j != 2:
                        std_dev = np.abs(value*std_width) #This is how big the fluctuation of the data should be. i.e "how much noise".
                        # print(std_dev)
                        noise = np.random.normal(value, std_dev,1)[0] #This creates some noise.
                        temp_row.append(noise)
                    else: 
                        temp_row.append(value)
                noise_Feats.append(temp_row)
                
            for row in Labels:
                temp_row = []
                for j,value in enumerate(row):
                    if j!=2:
                        std_dev = np.abs(value*std_width) #This is how big the fluctuation of the data should be. i.e how much noise.
                        noise = np.random.normal(value, std_dev,1)[0] #This creates some noise.
                        temp_row.append(noise)
                    else:
                        temp_row.append(value)
                    
                noise_Labs.append(temp_row)
        Features = Features + noise_Feats
        Labels = Labels + noise_Labs


    if leave_out is not None and LS02 == False:
        return Features, Labels,Feats_simple,names_selected_data ,test_features,test_labels
    
    return Features, Labels,Feats_simple,names_selected_data,None,None #Features and labels: Each row is a measurement, each collumn a variable


if __name__ == "__main__":
    LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data = Get_data("")
    LSKum001 = Fill_data(LSKum001);LSKum002 = Fill_data(LSKum002);LSKum005 = Fill_data(LSKum005);LSKum010 = Fill_data(LSKum010);
    LSKum050 = Fill_data(LSKum050);LSKum100 = Fill_data(LSKum100);LSKum020 = Fill_data(LSKum020)
    
    #The list below is "green stuff from excel UNION Gewasserschutzverordning GschV 3.2": Missing Cadmium and Molybdan from GschV
    some_list = ["pH","Leitfahigkeit","L/S kumuliert","Calcium","Arsenic", "Lead (Pb+2)","Copper",
                 "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"] #Missing: Molybdan
    # some_list = ["pH","L/S kumuliert", "Sulfate","Copper","Calcium"]
    
    
    LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data= Pick_data(LSKum001, LSKum002, LSKum005, LSKum010, LSKum020, LSKum050, LSKum100, names_selected_data, some_list)

    
    
    
    Feats, Labs,_,names = Make_Features_and_Labels(some_list, method="Steady",add_noise=False,std_width = 10,factor=3,add_extra = False,LS02 = False)
    Experiment_names = ["Nasschlacke 1","Nasschlacke 2","Nasschlacke 3","Nasschlacke 4","Nasschlacke 5",
                            "Nasschlacke 6","Nasschlacke 7","Nasschlacke 8","Nasschlacke 9",
                            "Mischung 1","Mischung 2","Mischung 3","Mischung 4","Mischung 5",
                            "Trockenschlacke 1","Trockenschlacke 2","Trockenschlacke 3","Trockenschlacke 4","Trockenschlacke 5",
                            "Trockenschlacke 6","Trockenschlacke 7","Trockenschlacke 8", 
                            "Magnetische Schlacke"]
    print(np.shape(Feats))
    Feats, Labs,_,names, F,L = Make_Features_and_Labels(some_list, method="Steady",add_noise=False,std_width = 10,factor=3,add_extra = False,LS02 = False,leave_out=Experiment_names[0] )
    print(np.shape(Feats))

            
            
            
            
            
