
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import difflib

from Read_Overall import Get_data
from Preprocessing_Overall import Fill_data
import seaborn as sns


LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data = Get_data("")
LSKum001 = Fill_data(LSKum001);LSKum002 = Fill_data(LSKum002);LSKum005 = Fill_data(LSKum005);LSKum010 = Fill_data(LSKum010);
LSKum050 = Fill_data(LSKum050);LSKum100 = Fill_data(LSKum100);LSKum020 = Fill_data(LSKum020)

'''
The point here is to find correlations through different plots. If some things are very correlated,
then that should be included as inputs. 
Main found correlations are of course with the LS-Values.
The LS values are plotted using method==Plot or scatter.
All the others using method == Correlation
Add the necessary elements to some_list.

'''

#The list below is "green stuff from excel UNION Gewasserschutzverordning GschV 3.2": Missing Cadmium and Molybdan from GschV
some_list = ["pH","Arsenic", "Lead (Pb+2)","Calcium","Leitfahigkeit","L/S kumuliert", "Copper",
             "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"] #Missing: Molybdan
some_list_wo_LS = ["pH","Sulfate","Copper","Calcium","Antimony","Manganese"]

Experiment_names = ["Nasschlacke 1","Nasschlacke 2","Nasschlacke 3","Nasschlacke 4","Nasschlacke 5",
                        "Nasschlacke 6","Nasschlacke 7","Nasschlacke 8","Nasschlacke 9",
                        "Mischung 1","Mischung 2","Mischung 3","Mischung 4","Mischung 5",
                        "Trockenschlacke 1","Trockenschlacke 2","Trockenschlacke 3","Trockenschlacke 4","Trockenschlacke 5",
                        "Trockenschlacke 6","Trockenschlacke 7","Trockenschlacke 8", 
                        "Magnetische Schlacke"]

def Pick_data(LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data, some_list,Normalize = False):
    '''
    In: Dataframe slices , some_list: List of strings, Normalize: Boolean
    Dataframe slices: The experimental data extracted from the experiments, see "Get_data"
    names_selected_data: List of strings with name of the elements we wish to work with. This is a list which has the most important elements for waste disposals.
    it is the uniuon of the most important elements by highlights in the excel file, as well as the elements found in the Gewaesserschutzverordnung by the Schweizerische Bundesrat
    Normalize: Whether or not we should divide the values by the value at 0.1 as a way to normalize the data. (Usually a bad idea)
    
    Out: The data with only the elements which are contained in the lsit. So if copper and lead are in the rows of the data,
    and only copper is in some_list, then only copper will be in the data going out. It will also normalize if Normalize is set to true.
    Normalization is done by dividing LSXXX by LS001
    '''

    
    #Try to make up for us being lazy, and writing e.g "Chrom" instead o "Chromium". We find the proper names to use.
    matches = []
    for name in some_list:
        close_match = difflib.get_close_matches(name, names_selected_data, n=1,cutoff = 0.6)
        if (len(close_match )!= 0):
            matches.append(close_match)
    indices = []
    for i in range(len(matches)):
        
        indices.append(np.where(np.array(names_selected_data) == matches[i])[0][0])
    # index_sulfate = np.where(np.array(names_selected_data) == matches[0])[0][0]
    # print(names_selected_data.iloc[indices])
    
    #Prepare the data
    LSKum001 = LSKum001.iloc[indices].reset_index(drop=True)
    LSKum002 = LSKum002.iloc[indices].reset_index(drop=True)
    LSKum005 = LSKum005.iloc[indices].reset_index(drop=True)
    LSKum010 = LSKum010.iloc[indices].reset_index(drop=True)
    LSKum020 = LSKum020.iloc[indices].reset_index(drop=True)
    LSKum050 = LSKum050.iloc[indices].reset_index(drop=True)
    LSKum100 = LSKum100.iloc[indices].reset_index(drop=True)
    names_selected_data = names_selected_data.iloc[indices].reset_index(drop=True)
    
    def is_similar_to_names(name, names_to_check):
        import difflib
        for target_name in names_to_check:
            if difflib.get_close_matches(name, [target_name], n=1, cutoff=0.6):
                return True
        return False
    
    #Normalize if wanted.
    if Normalize: #This can actually procude nans.
        names_to_skip = ['pH', 'Leitfähigkeit','L/S kumuliert']
        eps = 0.0001
        for i,name in enumerate(names_selected_data):
            if not is_similar_to_names(name,names_to_skip ):
                Values = LSKum001.iloc[i].values
                # print(Values)
                LSKum001.iloc[i] /= (Values+eps)
                LSKum002.iloc[i] /= (Values+eps)
                LSKum005.iloc[i] /= (Values+eps)
                LSKum010.iloc[i] /= (Values+eps)
                LSKum020.iloc[i] /= (Values+eps)
                LSKum050.iloc[i] /= (Values+eps)
                LSKum100.iloc[i] /= (Values+eps)
    return LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data



def Plot_data(x_axis,y_axis,LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data,method="scatter",LSBool="001"):
    '''


    Parameters
    ----------
    x_axis : String of what we wish to plot on xaxis, must be contained in names_selected_data 
    y_axis : ---"--- for y-axis
    LSKumXXX : Dataframe, The measurements at different LS values. 
    names_selected_data : Dataframe, Contains the elements etc; of what we wish to plot, that is, what we are actually interested in.
    method : String, How to plot: Scatter, Correlation or Plot, optional
        It will plot either scatter or regular plot using matplotlib, or in case of correlation use seaborns pairplot and .corr() from matplotlib. 
        The default is "scatter".
    LSBool : String, optional
        Which LS to plot from. The default is "001" which is simply the Liquid-Solid ratio of 0.1 (0.1x Liquid) Goes up to 10 (10x Liquid)
    #IMPORTANT: DUE TO THE WAY THE DATA HAS BEEN PREPROCESSED, PICKING METHOD = SCATTER WILL SEE CORRELATIONS BETWEEN "ELEMENT" and "LS CUMULATED"
    #IF METHOD == CORRELATION, THEN WE ARE ONLY CONSIDERING ONE LS VALUE, AND IT WILL LOOK LIKE A LINE IN THE PAIRPLOTS.
    Returns
    -------
    None.

    '''

    
    close_match_x = difflib.get_close_matches(x_axis, names_selected_data, n=1,cutoff = 0.6)
    x_index = np.where(np.array(names_selected_data) == close_match_x[0])[0][0]
    
    close_match_y = difflib.get_close_matches(y_axis, names_selected_data, n=1,cutoff = 0.6)

    y_index = np.where(np.array(names_selected_data) == close_match_y[0])[0][0]
    m,n = np.shape(LSKum001)
    
    if str(method) !="Correlation":
        LSKum = difflib.get_close_matches("L/S Kumuliert", names_selected_data, n=1,cutoff = 0.6)
        LSKum_index = np.where(np.array(names_selected_data) == LSKum[0])[0][0]
    
        
        temp_list = ['0.01','0.02','0.05','0.1','0.2','0.5','1']
        if close_match_x != LSKum and close_match_y != LSKum and method == "Plot":
            plt.figure()
            for j in range(n):
                temp_arr_x = []
                temp_arr_y = []
                for ind,LS in enumerate([LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100]):
                    temp_arr_x.append(LS.iloc[x_index,j])
                    temp_arr_y.append(LS.iloc[y_index,j])
                #HERE ADD IF NORMALIZE. TEMP_ARR/LS001
                plt.title(str(close_match_y)+ " vs " + str(close_match_x) )
                plt.ylabel(close_match_y)
                plt.xlabel(close_match_x)
                plt.plot(temp_arr_x,(temp_arr_y),label = j)
            # plt.legend()
            plt.grid()
            plt.show()
            
            # '''
            # I assume if none are LSKum, we want to do: Element 1 to element 2 at different LSKum-values
            # '''
        if close_match_x != LSKum and close_match_y != LSKum and method == "Scatter":
            plt.figure()
            for j in range(n):
                temp_arr_x = []
                temp_arr_y = []
                for ind,LS in enumerate([LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100]):
                    temp_arr_x.append(LS.iloc[x_index,j])
                    temp_arr_y.append(LS.iloc[y_index,j])
                plt.scatter(temp_arr_x,temp_arr_y)
                plt.xlabel(str(close_match_x)); plt.ylabel(str(close_match_y));plt.title(str(close_match_y) + " vs " + str(close_match_x));
                plt.grid(True)
                
                # plt.legend()
            plt.show()
                
                
        if close_match_x == LSKum and method == "Plot":
            plt.figure()
            for j in range(n):
                temp_arr_x = []
                temp_arr_y = []
                for ind,LS in enumerate([LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100]):
                    temp_arr_x.append(LS.iloc[x_index,j])
                    temp_arr_y.append(LS.iloc[y_index,j])
                
                plt.title(str(close_match_y) + " vs " + str(LSKum))
                plt.plot(temp_arr_x,(temp_arr_y),label = str(LSKum001.columns.values[j]))
            plt.legend(bbox_to_anchor=(1.05, 0), loc='lower left', borderaxespad=0., prop = {'size': 7})
            plt.grid(True)
            plt.ylabel(close_match_y)
            plt.xlabel(close_match_x)
            plt.show() 
            
        
        
        if close_match_x == LSKum and method == "Plot":
            plt.figure()
            for j in range(n):
                temp_arr_x = []
                temp_arr_y = []
                for ind,LS in enumerate([LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100]):
                    temp_arr_x.append(LS.iloc[x_index,j])
                    temp_arr_y.append(LS.iloc[y_index,j])
                
                plt.title(str(close_match_y) + " vs " + str(LSKum))
                if j in [11,12,13,14,15,16,17,18]:
                    plt.plot(temp_arr_x,(temp_arr_y),label = str(LSKum001.columns.values[j]))
            plt.legend(bbox_to_anchor=(1.05, 0), loc='lower left', borderaxespad=0., prop = {'size': 7})
            plt.grid(True)
            plt.ylabel(close_match_y)
            plt.xlabel(close_match_x)
            plt.show() 
        
        if close_match_x == LSKum and method == "Scatter":
            
        
            plt.figure()
            for j in range(n):
                temp_arr_x = []
                temp_arr_y = []
                for ind,LS in enumerate([LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100]):
                    temp_arr_x.append(LS.iloc[x_index,j])
                    temp_arr_y.append(LS.iloc[y_index,j])
                
                plt.title(str(LSKum) + " vs " + str(close_match_y))
                plt.scatter(temp_arr_x,temp_arr_y,label = j)
            # plt.legend()
            plt.ylabel(close_match_y)
            plt.xlabel(close_match_x)
            plt.show() 
    
    #Corr_x and Corr_y will just take the entire rows.
    Corr_x = []
    Corr_y = []
    

    
    dataframe_LS = pd.DataFrame()
    if method == "Correlation":
        for ind,LS in enumerate([LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100]):
            x_per_LS = []
            y_per_LS = []
            Correlation_matrix = []
            for j in range(n):
                Correlation_matrix = np.hstack(LS.iloc[:,j])
                
                Corr_x.append(LS.iloc[x_index,j])
                Corr_y.append(LS.iloc[y_index,j])

                x_per_LS.append(LS.iloc[x_index,j])
                y_per_LS.append(LS.iloc[y_index,j])
            dataframe_LS[str(ind) + str(close_match_x)] = x_per_LS
            dataframe_LS[str(ind) + str(close_match_y)] = y_per_LS
                        
        dataframe = pd.DataFrame({str(close_match_x): Corr_x,str(close_match_y): Corr_y}).transpose()
        
        Correlation_all = dataframe.corr()
        plt.matshow(Correlation_all)
        plt.title('All'); plt.xlabel(str(close_match_x)); plt.ylabel(str(close_match_y))
        plt.show()
        
        dataframe_LS = dataframe_LS
        selected_dataframe = dataframe_LS.iloc[[0,1]]
        plt.matshow(selected_dataframe.corr())
        plt.title('Per LS'); plt.xlabel(str(close_match_x)); plt.ylabel(str(close_match_y))
        # plt.matshow(dataframe_LS.corr())
        plt.show()
    
    if method == "Correlation":
        if LSBool == "001":
            LSK = LSKum001
        elif LSBool == "002":
            LSK = LSKum002
        elif LSBool == "005":
            LSK = LSKum005
        elif LSBool == "010":
            LSK = LSKum010
        elif LSBool == "020":
            LSK = LSKum020
        elif LSBool == "050":
            LSK = LSKum050
        elif LSBool == "100":
            LSK = LSKum100
        # print(LSK)
        LSK = LSK.transpose()
        LSK.columns = names_selected_data
        
        # print(LSK)
        # LSK.insert(0, "Names",names_selected_data)
        
        # LSK["Names"] = names_selected_data

        sns.pairplot(LSK) #Just a bunch of scatter plots
        
        # print(sns.load_dataset('iris'))
        # print(LSK)
        
    # print(dataframe_LS)
    # sns.pairplot(dataframe_LS)

    return 
# LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data= Pick_data(LSKum001, LSKum002, LSKum005, LSKum010, LSKum020, LSKum050, LSKum100, names_selected_data, some_list)
# print(LSKum001.iloc[0])

# print(names_selected_data)
# Plot_data("L/S Kumuliert","Copper",LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data,method="Plot",LSBool = "002")  

if __name__ == "__main__":
    print("Hello, World!")
    LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data= Pick_data(LSKum001, LSKum002, LSKum005, LSKum010, LSKum020, LSKum050, LSKum100, names_selected_data, some_list, Normalize = False)
    # print(LSKum001.iloc[:,0])
    # print(names_selected_data)
    for element in some_list:
        print(element)
        Plot_data("L/S Kumuliert",str(element),LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data,method="Plot",LSBool = "002")  
                
                
            
            
            
            
