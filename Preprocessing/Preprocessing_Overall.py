
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from Read_Overall import Get_data
import difflib

# LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data = Get_data("")

'''
TODO:
    This code will find a way to deal with the missing data. Currently, it takes in
    a dataframe with "follows" and "N/A" values. These are all interpreted as missing.
    
    These values are changed by doing the following:
    Take the non missing values, calculate their standard deviation. Use this std
    for gaussians (normal distributions) around the values given by the found values.
    Take a random point for each of these gaussians, and average over them to find
    a missing value. Repeat for each missing value.
    Eg: [1,2,NaN] -> \sigma = standard.dev(1,2) -> (Gauss(1,sigma)+ Gauss(2,sigma))/2 = NaN.
    
    'nn' values are changed according to "method".
'''

seed = 313 #We must set a random seed for reproducibility. 313 is Donald Ducks car number.
np.random.seed(seed)

def Fill_data(data,method = 'Half'): 
    '''
    

    Parameters
    ----------
    data : DataFrame.iloc: LSKumXXX, as can be seen from Get_data.
    method : String, optional
        How to deal with missing data. The default is 'Half'.
            Choose between zero, random and half. Based on the main paper, this has little influence. Zero means take zero, half means take half of the smallest measured value, 
            and random means take a random value between zero and smallest measured.

    Returns
    -------
    data : DataFrame.iloc
        Filled data, it has dealt with the missing data.

    '''
    m,n = np.shape(data) 
    # data.replace("follows",pd.NA,inplace=True) #I am not sure what "follows" means. But probably something like "TODO" and it was never done.
    # data.replace("n.a.",pd.NA,inplace=True)
    data.replace(to_replace=['.*follows.*', '\s*n\.a\.\s*', '\s*nan\s*','N/A','na'], value=pd.NA, regex=True, inplace=True)
    # print(names_selected_data)
    
    # close_matches = difflib.get_close_matches("Sulfur", names_selected_data, n=1) 
    

    for i in range(m):
        for j in range(n): #nn = Nicht nachwiesbar = not measurable
            if method == "Half": #If method == half, we take half the smallest measured value.
                if str(data.iloc[i,j]) == 'nn':
                    arr = pd.to_numeric(data.iloc[i,:], errors = 'coerce')
                    data.iloc[i,j] = min(arr.notna())/2
                    # print(min(arr.notna())/2)
            elif method == "Zero": #Just put nn's to zero
                if str(data.iloc[i,j]) == 'nn':
                    data.iloc[i,j] = 0
            elif method == "Random": #Replace nn with random val between 0 and smallest
                if str(data.iloc[i,j]) == 'nn':
                    arr = pd.to_numeric(data.iloc[i,:], errors = 'coerce')
                    temp = min(arr.notna())
                    temp = np.random.uniform(0,temp)
                    # print(temp)
                    data.iloc[i,j] = temp
                    
    for i in range(m):
        arr = (pd.to_numeric(data.iloc[i,:], errors = 'coerce')) #Example: [1,500,'319',200] Should be [1,500,319,200]
        
        non_nan_values = arr[arr.notna()]#Non-nan-values
        sigma = np.std(non_nan_values) #standard deviation
        
        if (arr.isna().any() == True):
            for j,k in enumerate (arr.isna()):
                if k == True :
                    random_samples = np.random.normal(loc = non_nan_values, scale = sigma) #This draws random values
                    arr[j] = np.sum(random_samples)/len(random_samples) #This averages over the random points.
        # print(arr)
        data.iloc[i,:] = arr
    
    
    
    return data


if __name__ == "__main__":
    LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data = Get_data("")
    FilledLSKum001 = Fill_data(LSKum001)

# print(FilledLSKum001.iloc[8,:])



            
            
            
            
            
            
            
            
