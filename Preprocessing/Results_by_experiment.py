import pandas as pd
import numpy as np
from Last_Preprocess_Overall import Make_Features_and_Labels

from Read_Overall import Get_data
from Preprocessing_Overall import Fill_data
from Pick_Overall import Pick_data
import difflib
import pysindy

def Sort_by_experiment(Experiment):
    '''
    

    Parameters
    ----------
    Experiment : str
        Name of the experiment, for example "Nasschlacke 1". It will be in the upper row of the csv file, as can be seen in the "Data" folder: Data/Saeulenversuche_Overall

    Returns
    -------
    Feats_df : pandas.DataFrame
        This will return the results ordered by the experiment, rather than ordered by the LS values. 


    '''
    # Get data from files
    LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data = Get_data("")
    
    # Fill missing data
    LSKum001 = Fill_data(LSKum001)
    LSKum002 = Fill_data(LSKum002)
    LSKum005 = Fill_data(LSKum005)
    LSKum010 = Fill_data(LSKum010)
    LSKum050 = Fill_data(LSKum050)
    LSKum100 = Fill_data(LSKum100)
    LSKum020 = Fill_data(LSKum020)

    # List of features to consider
    some_list = ["pH","Leitfahigkeit","L/S kumuliert","Calcium","Arsenic", "Lead (Pb","Copper",
                 "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride",
                 "Sulfate","Cadmium"] # Missing: Molybdan
    
    # Boolean variables for normalization
    Boolnorm = False
    Ramona_norm = False

    # Pick relevant data
    LSKum001, LSKum002, LSKum005, LSKum010,LSKum020,LSKum050,LSKum100,names_selected_data= Pick_data(LSKum001, LSKum002, LSKum005, LSKum010, LSKum020, LSKum050, LSKum100, names_selected_data, some_list, Normalize = Boolnorm)
    
    # List of experiment names
    Experiment_names = ["Nasschlacke 1","Nasschlacke 2","Nasschlacke 3","Nasschlacke 4","Nasschlacke 5",
                        "Nasschlacke 6","Nasschlacke 7","Nasschlacke 8","Nasschlacke 9",
                        "Mischung 1","Mischung 2","Mischung 3","Mischung 4","Mischung 5",
                        "Trockenschlacke 1","Trockenschlacke 2","Trockenschlacke 3","Trockenschlacke 4","Trockenschlacke 5",
                        "Trockenschlacke 6","Trockenschlacke 7","Trockenschlacke 8", 
                        "Magnetische Schlacke"]

    # Find the closest match to the given experiment name
    m,n = np.shape(LSKum001)
    closest_match = difflib.get_close_matches(Experiment, Experiment_names, n=1)

    if closest_match:
        Index_experiment = Experiment_names.index(closest_match[0])
    else:
        print("No close match found for:", Experiment)
        Index_experiment = None

    # Extract selected data for the experiment
    data_frames = [LSKum001, LSKum002, LSKum005, LSKum010, LSKum020, LSKum050, LSKum100]
    selected_data = []
    for df in data_frames:
        series = df.iloc[:, Index_experiment]
        series.index = some_list  # Set the index of the Series
        selected_data.append(series)

    # Create a DataFrame from the list of Series
    Feats_df = pd.DataFrame(selected_data, columns=some_list)
    return Feats_df

if __name__ == "__main__":
    # Sort data for "Nasschlacke 1" experiment and save to a CSV file
    Feat_df = Sort_by_experiment("Nasschlacke 1")
    print(np.shape(Feat_df))
    print((Feat_df.T))
    # List of experiment names
    Experiment_names = ["Nasschlacke 1","Nasschlacke 2","Nasschlacke 3","Nasschlacke 4","Nasschlacke 5",
                        "Nasschlacke 6","Nasschlacke 7","Nasschlacke 8","Nasschlacke 9",
                        "Mischung 1","Mischung 2","Mischung 3","Mischung 4","Mischung 5",
                        "Trockenschlacke 1","Trockenschlacke 2","Trockenschlacke 3","Trockenschlacke 4","Trockenschlacke 5",
                        "Trockenschlacke 6","Trockenschlacke 7","Trockenschlacke 8", 
                        "Magnetische Schlacke"]
    
    # Loop through all experiment names, sort data, and save to CSV files
    # for name in Experiment_names:
    #     Feat_df = Sort_by_experiment(name)
    #     Feat_df.to_csv(str(name)+".csv", index=False)

    