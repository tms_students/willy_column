import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
# sys.path.append(r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Preprocessing')


def Get_data(path):
    '''
    Reads data from an Excel file and extracts relevant information.

    Parameters
    ----------
    path : str
        Path of the Excel file.

    Returns
    -------
    LSKumXXX: DataFrame.iloc
        7 datas, sorted by their LS values. For example is LSKum002, the elements to the cumulated Liquid-Solid-ratio of 0.2
    names_selected_data: DataFrame.iloc
        names
        
    '''
    # sys.path.append(r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Preprocessing')
    # If no path is provided, use the default path
    script_dir = os.path.dirname(os.path.abspath(__file__))
    
    parent_dir = os.path.abspath(os.path.join(script_dir, os.pardir))
    if len(path) == 0:

        path = os.path.join(parent_dir, "Data", "Säulenversuche_Overall.xlsx")
        # path = os.path.join( "Data", "Säulenversuche_Overall.xlsx")
    # Read data from Excel file
    df = pd.read_excel(path)
    # Select relevant data
    Selected_data = df.iloc[1:34,:]
    names_selected_data = Selected_data.iloc[:,0]  # Names of the data
    
    # Calculate data length
    m, n = np.shape(Selected_data)
    Data_length = n - 1
    
    # Extract data for different L/S ratios
    offset = 0
    Ranges = np.arange(1, Data_length + offset, step=7)
    LSKum001 = Selected_data.iloc[:, Ranges]

    Ranges = np.arange(1, Data_length + offset, step=7) + 1
    LSKum002 = Selected_data.iloc[:, Ranges]

    Ranges = np.arange(1, Data_length + offset, step=7) + 2
    LSKum005 = Selected_data.iloc[:, Ranges]

    Ranges = np.arange(1, Data_length + offset, step=7) + 3
    LSKum010 = Selected_data.iloc[:, Ranges]

    Ranges = np.arange(1, Data_length + offset, step=7) + 4
    LSKum020 = Selected_data.iloc[:, Ranges]

    Ranges = np.arange(1, Data_length + offset, step=7) + 5
    LSKum050 = Selected_data.iloc[:, Ranges]

    Ranges = np.arange(1, Data_length + offset, step=7) + 6
    LSKum100 = Selected_data.iloc[:, Ranges]

    return LSKum001, LSKum002, LSKum005, LSKum010, LSKum020, LSKum050, LSKum100, names_selected_data

if __name__ == "__main__":
    # sys.path.append(r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Preprocessing')
    # Get data for different L/S ratios
    LS001, LS002, LS005, LS010, LS020, LS050, LS100, names = Get_data("")
