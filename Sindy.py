import pysindy as ps
from pysindy.optimizers import STLSQ
import pysindy.feature_library as fl
from pysindy.feature_library import ConcatLibrary,CustomLibrary,GeneralizedLibrary

polynomial_library = fl.PolynomialLibrary(degree=3)
identity_library = fl.IdentityLibrary()
fourier_library = fl.FourierLibrary(n_frequencies=3)
feature_library = ConcatLibrary([polynomial_library, identity_library])

import numpy as  np
from Results_by_experiment import Sort_by_experiment
from sklearn.preprocessing import MinMaxScaler,StandardScaler

from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import difflib
from scipy.integrate import odeint 
from sklearn.metrics import r2_score

some_list = ["pH","Leitfahigkeit","L/S kumuliert","Calcium","Arsenic", "Lead (Pb","Copper",
          "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"]
#Here we assume that the amplitudes are describing the rate of change, rather than the "amplitudes"


def Sindy(Index_of_experiment_name,Index_of_experiment_to_predict,Experiment_names,Element_to_predict, Elements_as_input):
    '''
    

    Parameters
    ----------
    Index_of_experiment_name : This is the data used for fitting [Integer]
    Index_of_experiment_to_predict : We predict on this data     [Integer]
    Experiment_names : List of experiment names.                [list of strings]
    Element_to_predict : Name of the element we wish to predict  [String]
    Elements_as_input : Which elements are important for predics [List  of strings]

    Returns
    -------
    Different types of errors as well as the predictions for Element to predict: [n_times]

    '''
    print("Currently doing Sindy...")
    #Define inputs:
    index_of_element_to_predict = find_index(Element_to_predict, some_list)
    indices_for_input = []
    for name in Elements_as_input:
        indices_for_input.append(find_index(name, some_list))
    index_of_element_to_predict_experiment = find_index(Element_to_predict, Elements_as_input)
    times = Sort_by_experiment(Experiment_names[Index_of_experiment_name]).iloc[:,2].values
    X = Sort_by_experiment(Experiment_names[Index_of_experiment_name]).iloc[:,indices_for_input].values
    
    X2 = Sort_by_experiment(Experiment_names[Index_of_experiment_to_predict]).iloc[:,indices_for_input].values #We try to guess this element.

    #Define  the library. 
    custom_library = ConcatLibrary([CustomLibrary(library_functions=[Law_of_Mass_Action,Another_Law]),polynomial_library])
    #Define how to scale the data.
    scaler = StandardScaler()
    # scaler= MinMaxScaler()
    X = scaler.fit_transform(X)
    model = ps.SINDy(optimizer=None, feature_library=custom_library, feature_names= Elements_as_input)
    
    
    model.fit(X,t = times) #We fit on "Experiment"
    x0=X[:,index_of_element_to_predict_experiment][0] #Define starting values for fit data for OWN integrators
    x02=X2[:,index_of_element_to_predict_experiment][0] #Define starting values for the date which we want to predict for own integrators
    # print(model.score(X))
    # print(model.print()) #Here you can see how the model thinks.
    y0 = X[0,:]#Define starting values for fit data. Used by sindy
    y02 =X2[0,:]#Define starting values for the date which we want to predict. Used by sindy
    print("Simulating...")
    sindy_simulated = model.simulate(y0, times, integrator='solve_ivp', integrator_kws={'atol': 1e-12, 'method': 'LSODA', 'rtol': 1e-12})

    print("Simulating (1) done!")
    
    sindy_simulated_2 = model.simulate(y02,times, integrator='solve_ivp', integrator_kws={'atol': 1e-12, 'method': 'LSODA', 'rtol': 1e-12})
    print("Simulating (2) done!")
    # print(np.shape(model.coefficients()))
    
    time_derivatives = model.predict(X)[:,index_of_element_to_predict_experiment]

    # print(time_derivatives)
    # print(x0)
    # print(X[:,index_of_element_to_predict_experiment])
    plt.figure()
    plt.title(str(Element_to_predict))
    plt.scatter(times, X[:,index_of_element_to_predict_experiment])
    # plt.plot(times, Simpson_predict(x0, time_derivatives, times),label = "Simpson")
    # plt.plot(times,Euler_predict(x0, time_derivatives, times),label = "Euler")
    # plt.plot(times,Trapezoid_predict(x0, time_derivatives, times),label = "Trapezoid")
    plt.plot(times, sindy_simulated[:,index_of_element_to_predict_experiment], label = "Sindy")
    # plt.plot(times,model.predict(X)[:,index_of_element_to_predict_experiment], label = "Model.predict")
    plt.legend()
    plt.grid(True)
    plt.show()
    # plt.figure()
    # plt.scatter(times, X2[:,index_of_element_to_predict_experiment])
    # plt.plot(times, Simpson_predict(x02, time_derivatives2, times),label = "Simpson")
    # plt.plot(times,Euler_predict(x02, time_derivatives2, times),label = "Euler")
    # plt.plot(times,Trapezoid_predict(x02, time_derivatives2, times),label = "Trapezoid")
    # plt.plot(times, sindy_simulated_2[:,index_of_element_to_predict_experiment], label = "Sindy")
    # plt.legend()
    # plt.grid(True)
    # plt.show()
    # print("R2 Trapezoid ", r2_score(Trapezoid_predict(x0, time_derivatives, times),X[:,index_of_element_to_predict_experiment]))
    
    # print("R2 Model.Predict", r2_score(model.predict(X)[:,index_of_element_to_predict_experiment],X[:,index_of_element_to_predict_experiment]))
    # print("R2 Sindy ", r2_score(sindy_simulated[:,index_of_element_to_predict_experiment],X[:,index_of_element_to_predict_experiment]))
    # print("R2 Trapezoid ", r2_score(Trapezoid_predict(x02, time_derivatives, times),X2[:,index_of_element_to_predict_experiment]))
    # print("R2 Sindy ", r2_score(sindy_simulated_2[:,index_of_element_to_predict_experiment],X2[:,index_of_element_to_predict_experiment]))
    actual = X[:,index_of_element_to_predict_experiment]
    predicted = sindy_simulated[:,index_of_element_to_predict_experiment]
    Rel,WMAP, r2 = Errors(actual,predicted)
    
    actual2 = X2[:,index_of_element_to_predict_experiment]
    predict2 = sindy_simulated_2[:,index_of_element_to_predict_experiment]
    Rel2,WMAP2, r22 = Errors(actual2,predict2)
    #We return how well it did on own data, and how well it tried to predict new data.
    return Rel,WMAP,r2, sindy_simulated[:,index_of_element_to_predict_experiment],Rel2,WMAP2,r22, sindy_simulated_2[:,index_of_element_to_predict_experiment]


def Trapezoid_predict(Start_value, Derivatives,times):
    Length = len(Derivatives)
    output = np.zeros(Length)
    dts =  (np.diff(times))
    output[0] = Start_value
    for i in range(Length-1):
        output[i+1] = output[i] + (Derivatives[i]+Derivatives[i+1] ) *dts[i]/2
    return output

def Euler_predict(Start_value, Derivatives,times):
    Length = len(Derivatives)
    output = np.zeros(Length)
    dts =  (np.diff(times))
    output[0] = Start_value
    for i in range(Length-1):
        output[i+1] = output[i] + (Derivatives[i]) *dts[i]
    return output

def Simpson_predict(Start_value, Derivatives,times):
    Length = len(Derivatives)
    output = np.zeros(Length)
    dts =  (np.diff(times))
    output[0] = Start_value
    output[1] = output[0] + (Derivatives[0]+Derivatives[1]) *dts[0]/2
    for i in range(Length-2):
        output[i+2] = output[i] + (Derivatives[i]+Derivatives[i+1]*4 + Derivatives[i + 2]) *dts[i]/3
    return output

def find_index(input_str, search_list):
    matches = difflib.get_close_matches(input_str, search_list, n=1, cutoff=0.6)
    if matches:
        match = matches[0]
        index = search_list.index(match)
        return index
    else:
        return None
def Law_of_Mass_Action(x):
    return np.vstack([np.exp(-x)])
def Another_Law(x):
    return np.sin(x)

def Errors(actual, predicted):
    def wmape(actual, pred):
        actual = np.array(actual)
        pred = np.array(pred)
        wMAPE = np.sum(np.abs(actual-pred))/np.sum(np.abs(actual))*100
        return wMAPE
    
    def Relative_loss(actual,pred):
        norm = len(actual)
        actual = np.array(actual)
        pred = np.array(pred)
        Relative_loss = np.sum(np.abs(actual-pred)/(np.abs(actual)))/norm
        return Relative_loss
    Rel = Relative_loss(actual, predicted)
    WMAP = wmape(actual,predicted)
    r2 = r2_score(actual, predicted)
    return Rel,WMAP,r2
if __name__ == "__main__":
    some_list = ["pH","Leitfahigkeit","L/S kumuliert","Calcium","Arsenic", "Lead (Pb","Copper",
              "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"]
    
    
    Experiment_names = ["Nasschlacke 1","Nasschlacke 2","Nasschlacke 3","Nasschlacke 4","Nasschlacke 5",
                        "Nasschlacke 6","Nasschlacke 7","Nasschlacke 8","Nasschlacke 9"]
    
    Experiment_names = ["Trockenschlacke 1","Trockenschlacke 2","Trockenschlacke 3","Trockenschlacke 4","Trockenschlacke 5",
    "Trockenschlacke 6","Trockenschlacke 7","Trockenschlacke 8"]
    Relatives = []
    WMAPS = []
    R2s = []
    for index, name in enumerate(Experiment_names):
        for index2, name2 in enumerate(Experiment_names):
            if index2 == index:
                continue
            try:
                Rel,WMAP,r2, sols,Rel2,WMAP2,r22, sols2= Sindy(index2,index,Experiment_names,"Chloride",["Chloride","Sulfate"])
                print(Rel2,WMAP2,r22)
                Relatives.append(Rel2)
                WMAPS.append(WMAP2)
                R2s.append(r22)
            except Exception as e:
                print("Almost certainly this stupid infinity error...Indices:",index,index2)
            
    Relative_error = np.mean(Relatives)
    WMAP_error = np.mean(WMAPS)
    R2_error = np.mean(R2s)
    print(Relative_error,WMAP_error,R2_error)
    
    
    
    