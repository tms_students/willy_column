import numpy as np
import torch
import random
import sys
import re

# sys.path.append(r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Preprocessing')
sys.path.append('Preprocessing')
# Your existing code
from Last_Preprocess_Overall import Make_Features_and_Labels
import torch.nn as nn
from sklearn.model_selection import KFold
import torch.optim as optim
from torch.utils.data import DataLoader, TensorDataset
import torch.nn.functional as F
from sklearn.metrics import r2_score
from sklearn.preprocessing import MinMaxScaler,StandardScaler, RobustScaler,FunctionTransformer
import difflib
from sklearn.datasets import load_iris #Just to check
import inspect
import shap
import pandas as pd

import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error,mean_absolute_percentage_error
import inspect

from torchviz import make_dot
from torchsummary import summary

def Machine_Learning(Features, Labels,Fleftout,Lleftout,names,K_folds,seed,neurons=None,layers=None,std_width = 0.1, add_noise = True, factor = 5,name = "Copper (Cu)", reduced_input = True,Archit = False,LS_value=None,save_plot=False,Leftout_name=None,results_folder=None,pick_inputs=None,feature_engineering=False,first_three=True,first_two=False):
    '''
    Parameters
    ----------
    Features : Features for the model
    Labels : Labels for the model
    K_folds : How many folds using kfolds
    std_width : if add_noise==True: Create new data, std_width=0.01 draws gaussian samples about 
    other values with std_dev 0.01. The default is 0.01.
    add_noise : Boolean. DEfault = True
    factor : Integer. Factor by how much should we increase data? if 3, our dataset will consist of original,
    plus 3 with added noise, making the training data 4 times as big. The default is 3.
    names: List of names in the data, like Calcium, L/S, etc;
    name: The target label. Model will only predict the name of this label.

    Returns
    -------
    x : TYPE
        DESCRIPTION.

    '''
    print("")
    print("Currently fitting for (Neural Net): ",str(name), "Using seed: ",str(seed))
    shape_1 = np.shape(Features) 
    shape_2 = np.shape(Labels)
    


    # Define the number of folds
    k_folds = K_folds
    
    # Initialize the KFold object
    kf = KFold(n_splits=k_folds, shuffle = True, random_state=seed)
    
    # Initialize lists to store the R-squared scores for each fold
    r2_scores = []
    wmap_score=[]
    Relative_score = []
    
    r2_scores_train = []
    wmap_score_train = []
    Relative_score_train = []
    RMSE_train = []
    RMSE_test = []
    
    r2_score_leave_out = []
    wmap_leave_out = []
    Relative_score_leave_out = []
    RMSE_leave_out = []
    # Convert feature matrix and labels to numpy array
    Feature_Matrix_ = np.array(Features)
    Label_Vector_ = np.array(Labels)
    Lleftout_copy = np.array(Lleftout)
    Fleftout_copy = np.array(Fleftout)
    # Initialize MinMaxScaler
    
    if LS_value is not None: #Remove the training for small LS values.
        if isinstance(LS_value, list):
            mask = ~np.isin(Label_Vector_[:,2], LS_value)  # Inverse the condition
            Leftmask = ~np.isin(Lleftout_copy[:,2], LS_value)
        else:
            mask = (Label_Vector_[:,2] == LS_value)  # Inverse the condition
            Leftmask = (Lleftout_copy[:,2] == LS_value) 
        # mask = (Label_Vector_[:,2] != LS_value)
        Feature_Matrix_ = Feature_Matrix_[mask]
        Label_Vector_ = Label_Vector_[mask]
        
        Lleftout_copy = Lleftout_copy[Leftmask]
        Fleftout_copy = Fleftout_copy[Leftmask]
    folder_counter = 0
    # Create DataLoader for training and testing
    epsilon = 1.123e-4 #1e-7 is much smaller than anything else in the data set
    # scaler = FunctionTransformer(lambda x: np.log(np.abs(x + epsilon)), validate=True)
    # scaler = MinMaxScaler() #C = (c_i - c_min)/(c_max - c_min) -> Range in [0,1]. It creates inf for Konz/LS! Careful! (add epsilon)
    
    # scaler=StandardScaler()
    scaler=RobustScaler()
    combined_features = np.vstack((Feature_Matrix_, Fleftout_copy))

    scaled_combined_features = scaler.fit_transform(combined_features) 
    Feature_Matrix_Before_Scaling = Feature_Matrix_
    Feature_Matrix_ = scaled_combined_features[:Feature_Matrix_.shape[0]]
    Fleftout_trans_1 = scaled_combined_features[Feature_Matrix_.shape[0]:]

    nan_check = np.isnan(Feature_Matrix_)
    print("NaNs in Scaled Feature Matrix:", nan_check.any())
    print("Maximal value in Feature Matrix:",np.max(Feature_Matrix_))

    #Cross validation (kfolds):
    for train_index, test_index in kf.split(Feature_Matrix_):
        X_train_fold, X_test_fold = Feature_Matrix_[train_index], Feature_Matrix_[test_index]
        y_train_fold, y_test_fold = Label_Vector_[train_index], Label_Vector_[test_index]
        
        untransformed_train, untransformed_test = Feature_Matrix_Before_Scaling[train_index],Feature_Matrix_Before_Scaling[test_index]
        folder_counter+=1
        # if folder_counter != 1:
        #     continue
        #Here we should make more data from noise on the train folds. Perhaps this will give us convergence.
        Feat_list = list(X_train_fold)
        Lab_list = list(y_train_fold)
        Fleftout_arr = Fleftout_trans_1
        
        if add_noise: #This adds noisy data to the testing folds, to create more to train on.
            #Here we should make more data from noise on the train folds. Perhaps this will give us convergence.
            Feat_list = list(X_train_fold)
            Lab_list = list(y_train_fold)
            m,n = np.shape(Feat_list)
            noise_Feats = []
            noise_Labs = []
            for times in range(factor):
                for row in Feat_list: #This moves row by row
                    temp_row = []
                    for value in row: #This moves along the row
                        # std_dev = np.abs(value*std_width) #This is how big the fluctuation of the data should be. i.e "how much noise".
                        # print(std_dev)
                        std_dev = std_width
                        noise = np.random.normal(value, std_dev,1)[0] #This creates some noise.
                        temp_row.append(noise)
                    noise_Feats.append(temp_row)
                for row in Lab_list:
                    temp_row = []
                    for value in row:
                        # std_dev = np.abs(value*std_width) #This is how big the fluctuation of the data should be. i.e how much noise.
                        std_dev = std_width
                        noise = np.random.normal(value, std_dev,1)[0] #This creates some noise.
                        temp_row.append(noise)
                    
                    noise_Labs.append(temp_row)
            Feat_list = Feat_list + noise_Feats
            Lab_list = Lab_list + noise_Labs
            
            X_train_fold = np.array(Feat_list)
            y_train_fold = np.array(Lab_list)
        

        if len(name)!=0:
            close_match = difflib.get_close_matches(name + "LS01", names, n=1,cutoff = 0.6)
            if len(close_match) == 0:
                close_match = difflib.get_close_matches(name, names, n=1,cutoff = 0.6)#-3 since first three are removed from labels, and names has len(features)
            index = (np.where(np.array(names) == close_match)[0][0])-3 #-3 since first three are removed from labels, and names has len(features)
            y_train_fold = y_train_fold[:,index+3]
            y_train_fold = y_train_fold.reshape(-1,1)
            y_test_fold = y_test_fold[:,index+3]
            y_test_fold = y_test_fold.reshape(-1,1)
            
            Lleftout_arr = Lleftout_copy[:,index+3]
            Lleftout_arr = Lleftout_arr.reshape(-1,1)

            # shape_2 = np.shape(y_train_fold)
            if reduced_input: #This will make only LS, Leitf, LS*elem, plus element of output to input.
                # X_train_fold = np.concatenate((X_train_fold[:, :3], X_train_fold[:, index+3].reshape(-1, 1)), axis=1)
                X_train_fold = np.concatenate((X_train_fold[:, :3], X_train_fold[:, index+3].reshape(-1, 1), (X_train_fold[:, 2] * X_train_fold[:, index+3]).reshape(-1, 1)), axis=1)
                # X_train_fold = X_train_fold.reshape(-1,1)
                # X_test_fold = np.concatenate((X_test_fold[:, :3], X_test_fold[:, index+3].reshape(-1, 1)), axis=1)
                X_test_fold = np.concatenate((X_test_fold[:, :3], X_test_fold[:, index+3].reshape(-1, 1), (X_test_fold[:, 2] * X_test_fold[:, index+3]).reshape(-1, 1)), axis=1)
                # shape_1 = np.shape(X_train_fold)
                Fleftout_arr = np.concatenate((Fleftout_arr[:, :3], Fleftout_arr[:, index+3].reshape(-1, 1), (Fleftout_arr[:, 2] * Fleftout_arr[:, index+3]).reshape(-1, 1)), axis=1)

                string_to_add = "LS*"+str(name)
                string_series = pd.Series([string_to_add])
                a_string_series = pd.Series([str(name)])
                namese = pd.concat([names[:3], a_string_series,string_series])
                namese=namese.reset_index(drop=True)
            else:
                if feature_engineering:

                    X_train_fold = np.concatenate((X_train_fold, (X_train_fold[:, 2] * X_train_fold[:, index+3]).reshape(-1, 1).reshape(-1, 1),(1/X_train_fold[:, 2] * X_train_fold[:, index+3]).reshape(-1, 1),(np.exp(-X_train_fold[:, 2] * X_train_fold[:, index+3])).reshape(-1, 1)), axis=1)

                    X_test_fold = np.concatenate((X_test_fold, (X_test_fold[:, 2] * X_test_fold[:, index+3]).reshape(-1, 1).reshape(-1, 1),(1/X_test_fold[:, 2] * X_test_fold[:, index+3]).reshape(-1, 1),(np.exp(-X_test_fold[:, 2]) * X_test_fold[:, index+3]).reshape(-1, 1)), axis=1)
                    Fleftout_arr = np.concatenate((Fleftout_arr, (Fleftout_arr[:, 2] * Fleftout_arr[:, index+3]).reshape(-1, 1).reshape(-1, 1),(1/Fleftout_arr[:, 2] * Fleftout_arr[:, index+3]).reshape(-1, 1),(np.exp(-Fleftout_arr[:, 2] * Fleftout_arr[:, index+3])).reshape(-1, 1)), axis=1)
                    string_to_add = "LS*"+str(name)
                    string_to_add_2 = "LS/"+str(name)
                    string_to_add_3 = "exp(-"+str(name) + ')'
                    string_series = pd.Series([string_to_add,string_to_add_2,string_to_add_3])
                    namese = pd.concat([names, string_series])
                    namese=namese.reset_index(drop=True)
                if feature_engineering == False:
                    # X_train_fold = np.concatenate((X_train_fold, (X_train_fold[:, 2] * X_train_fold[:, index+3]).reshape(-1, 1).reshape(-1, 1)), axis=1)
                    # X_test_fold = np.concatenate((X_test_fold, (X_test_fold[:, 2] * X_test_fold[:, index+3]).reshape(-1, 1).reshape(-1, 1)), axis=1)
                    # # shape_1 = np.shape(X_train_fold)
                    # Fleftout_arr = np.concatenate((Fleftout_arr, (Fleftout_arr[:, 2] * Fleftout_arr[:, index+3]).reshape(-1, 1).reshape(-1, 1)), axis=1)
                    
                    # string_to_add = "LS*"+str(name)
                    
                    # string_series = pd.Series([string_to_add])
                    namese = pd.concat([names])
                    namese=namese.reset_index(drop=True)
            nameinput = name
            Fleftout_trans= Fleftout_arr
            if pick_inputs is not None:
                def get_closest_indices(names, names_to_keep):
                    closest_indices = []
                    for name in names_to_keep:
                        
                        closest_match1 = difflib.get_close_matches(name, names, n=1, cutoff=1) #Exact if L/S Kumuliert, LS*Calcium, pH , etc;
                        if closest_match1:
                            closest_indices.append(np.where(names == closest_match1[0])[0][0])
                        else:
                            # Find the closest base name in the names array (ignoring LSX)
                            closest_match = difflib.get_close_matches(name, names, n=3, cutoff=0.4)
                            if name == nameinput:
                                closest_match = difflib.get_close_matches(name, names, n=4, cutoff=0.4) #Because also LS*element exists. So there are 4.
                                if feature_engineering:
                                    closest_match = difflib.get_close_matches(name, names, n=6, cutoff=0.5)
                            # print("Name:", name)
                            # print("Base Names:", base_names)
                            
                            if closest_match:
                                for matches in closest_match:
                        
                                    closest_indices.append(np.where(names == matches)[0][0])
                                    

                        if first_three == False and first_two==False:
                            closest_indices = []
                            for name in names_to_keep:
                                closest_match1 = difflib.get_close_matches(name, names, n=1, cutoff=1) #Exact if L/S Kumuliert, LS*Calcium, pH , etc;
                                if closest_match1:
                                    closest_indices.append(np.where(names == closest_match1[0])[0][0])
                                else:
                                    closest_match = difflib.get_close_matches(name, names, n=3, cutoff=0.7)
                                    if closest_match:
                                        for matches in closest_match:
                                            closest_indices.append(np.where(names == matches)[0][0])
                                    
                    return closest_indices, pick_inputs
                
                indices_to_use, _ = get_closest_indices(namese, pick_inputs)
                namese = namese[indices_to_use].reset_index(drop=True)
                
                Fleftout_trans = Fleftout_trans[:,indices_to_use]
                X_train_fold=X_train_fold[:,indices_to_use]
                X_test_fold = X_test_fold[:,indices_to_use]

        if folder_counter==1:
            print("We will use: ",namese, "as our input")
        # Scale the features and labels
        
        shape_1 = np.shape(X_train_fold) 
        shape_2 = np.shape(y_train_fold)

        

        
        

        # print(np.shape(X_train_fold))
        # print(np.shape(X_test_fold))
        # y_train_fold = scaler.fit_transform(y_train_fold.reshape(-1, 1)).reshape(-1)
        # y_test_fold = scaler.transform(y_test_fold.reshape(-1, 1)).reshape(-1)

        # Convert data to torch tensors
        X_train_tensor = torch.tensor(X_train_fold, dtype=torch.float32)
        y_train_tensor = torch.tensor(y_train_fold, dtype=torch.float32)
        X_test_tensor = torch.tensor(X_test_fold, dtype=torch.float32)
        y_test_tensor = torch.tensor(y_test_fold, dtype=torch.float32)
        Fleftout_tensor = torch.tensor(Fleftout_trans,dtype=torch.float32)
        Lleftout_tensor = torch.tensor(Lleftout_arr,dtype=torch.float32)
        # print(np.shape(X_train_fold),np.shape(X_test_fold), np.shape(Fleftout_trans))
        # quit()
        # Create DataLoader for training
        train_loader = DataLoader(TensorDataset(X_train_tensor, y_train_tensor), batch_size=16, shuffle=True)
        test_loader = DataLoader(TensorDataset(X_test_tensor, y_test_tensor), batch_size=16)
        class NegELU(nn.Module):
            def __init__(self):
                super(NegELU, self).__init__()
                self.elu = nn.ELU()
            
            def forward(self, x):
                return self.elu(-x)
        
        # Define the model
        class Net(nn.Module):
            """
            The model class, which defines our feature extractor used in pretraining.
            """
    
            def __init__(self, neurons, layers):
                """
                The constructor of the model.
                """
                super(Net,self).__init__()
                # Define the architecture of the model.
                self.layers = nn.Sequential()
                self.layers.add_module("Starting layer:",nn.Linear(shape_1[1],neurons[0]))
                neuron_length = len(neurons)
                for i in range(1,(neuron_length)):
                    self.layers.add_module(f'hidden_layer_{i}', nn.Linear(neurons[i-1], neurons[i]))
                    self.layers.add_module(f'hidden_activation_{i}', nn.LeakyReLU(0.01))
                    self.layers.add_module(f'Elu_activation_{i}',NegELU())
                    self.layers.add_module(f'dropout_{i}', nn.Dropout(p=0.3))
                self.layers.add_module('hidden_activation_output', nn.LeakyReLU(0.01))
                self.layers.add_module('Elu_activation_output',NegELU())
                self.layers.add_module('dropout_output', nn.Dropout(p=0.3))
                self.layers.add_module('output_layer',nn.Linear(neurons[-1],out_features=shape_2[1]))
                
                
                
                self.weight_layer = nn.Linear(shape_2[1],shape_2[1])
                self.dropout = nn.Dropout(p=0.3)
                self.leaky = nn.LeakyReLU(0.01)

                
                self.trainable_constant = nn.Parameter(torch.randn(1)) 
                
            def forward(self, x):

                x = self.layers(x)

                return x #+ self.trainable_constant#By extension, this should then look like Aexp(-bx) + c
            
            def get_architecture(self):
                architecture = []
                forward_source = inspect.getsource(self.forward)
                forward_lines = forward_source.split('\n')[1:]  # Skip the first line (def forward(self, x):)
                
                

                architecture = []
                forward_source = inspect.getsource(self.forward)
                # Add the source code of the forward method to architecture
                architecture.append("Forward Method:")
                architecture.extend(forward_source.split('\n')[:])  # Skip the first line (def forward(self, x):)
                
                return architecture
                # return x

            
        # Create an instance of the model
        if neurons is None:
            neurons = [120,90,20]
        if layers is None:
            layers = 3

        model = Net(neurons=neurons, layers=layers)
        

        # Define loss function and optimizer
        criterion = nn.SmoothL1Loss()   #nn.SmoothL1Loss(), nn.MSELoss() 
        # criterion = nn.MSELoss() 
        learning_rate = 0.001 #Was 0.001
        
        optimizer = optim.SGD(model.parameters(), lr=learning_rate) #Setting this high makes "grads*lr" too large, eventually creating infs when using MSELoss
    
    
        scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.25, patience=2500, verbose=False) #Verbose prints.
        # Train the model
        num_epochs = 7500
        large_number = 15000
        if Archit == True:#Just get the architecture.
            avg_r2_score,MSE_loss,Arch = 0,0, model.get_architecture()
            Arch.append(f"Number of epochs: {num_epochs}")
            Arch.append(f"Learning rate: {learning_rate}")
            Arch.append(str(model))
            # print(Arch)
            return avg_r2_score,MSE_loss,None,None,None,None,None,None,None,Arch
        

        
        
        #Since the model may overshoot towards the end of the training,we keep track of the best model as it goes.
        best_r2_score = -np.inf
        best_model_state = None
        epoch = 0
        epoch_losses = []
        epoch_losses_test = []
        R2_losses = []
        list_of_epochs = []
        # Place this code block outside your loop as well
        string_element = str(name)
        string_seed = str(seed)
        string_folder_counter = str(folder_counter)
        if results_folder is None:
            results_folder = os.path.dirname(os.path.abspath(__file__))
            #results_folder = os.path.join("..","A_Results_1_noise", "Neural_Network")
        base_dir = os.path.join(results_folder,"Dictionaries")
        
        if not os.path.exists(base_dir):
            os.makedirs(base_dir)
        # model_save_path = r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\A_Results\Dictionaries\Calcium.pt'
        model_save_path = os.path.join(base_dir, f'{string_element}_{string_seed}_{string_folder_counter}_{Leftout_name}.pt')
        #model_save_path = r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\E_Results\Dictionaries\ ' + string_element + '_'+string_seed + '_' + string_folder_counter + '.pt'
        
        while epoch < (num_epochs): #Changed this to while, since stuff must be dynamically changed.
            epoch+=1
            model.train()  # Set the model to training mode.
            running_loss = 0.0
            
            for inputs, labels in train_loader:  # Iterate over the training data
                optimizer.zero_grad()  # Zero the gradients.
                outputs = model(inputs)
                loss = criterion(outputs, labels)  # Define the loss.
                loss.backward()  # Backward propagation.
                optimizer.step()  # Update parameters.
                running_loss += loss.item() * inputs.size(0)
                # print(running_loss)
            epoch_loss = running_loss / len(train_loader.dataset)
            if epoch % 10 == 0:
                # print(f"Epoch [{epoch + 1}/{num_epochs}], Loss: {epoch_loss:.4f}")
                test_loss = 0.0
                y_true = []
                y_pred = []
                running_loss_test = 0.0
                with torch.no_grad(): #The model is NOT training on this. This is on the validation.
                    for inputs, labels in test_loader:
                        outputs = model(inputs)
                        test_loss += criterion(outputs, labels).item() * inputs.size(0)
                        y_true.extend(labels.numpy())
                        y_pred.extend(outputs.numpy())
                        running_loss_test += test_loss* inputs.size(0)
                    epoch_test_loss = running_loss_test/ len(test_loader.dataset)

                r2 = r2_score(y_true, y_pred)
                epoch_losses.append(epoch_loss)
                epoch_losses_test.append(epoch_test_loss)
                R2_losses.append(r2)
                list_of_epochs.append(epoch)
                # to_be_printed = np.round(r2_score(y_true, y_pred),3)
                if r2>best_r2_score and epoch > num_epochs*0.95:
                    num_epochs = int(1.2*num_epochs )
                    # print(f"\rConvergence reached at epoch {epoch + 1}. Extending training for {int(num_epochs - epoch)} additional epochs.")
                    
                    
                if r2 > best_r2_score:
                    epoch_loss = running_loss / len(train_loader.dataset)
                    to_be_printed = np.round(r2_score(y_true, y_pred),3)
                    # sys.stdout.write('\r')
                    #print(f"\rEpoch [{epoch + 1}/{num_epochs}], R2-Loss: {to_be_printed}, CritLoss: {epoch_loss:.4f}",end="")
                    last_epoch = epoch
                    last_Crit = epoch_loss
                    best_r2_score = r2
                    best_model_state = model.state_dict()
                    try:
                        with open(model_save_path,'wb') as f: #I do not understand why this needs to be saved everytime.
                            try:
                                torch.save(best_model_state, f)#But this cannot be moved outside of the loop. No idea why.
                            except Exception as e:
                                # print("An error occurred while saving the model state:", e)
                                print("E1")
                    except Exception as e:
                        print("E1")
                        # print("An error occurred while saving the model state:", e)
                        
                print(f"\rEpoch [{last_epoch + 1}/{num_epochs}], R2-Loss: {to_be_printed}, CritLoss: {last_Crit:.4f}, Current epoch: {epoch}, current loss:{epoch_loss:.4f} ",end = '\r')
            if epoch > large_number:
                print(f"After [{large_number}] epochs, we have really tried hard enough.")
                epoch = num_epochs
        if best_model_state is not None:
            # print(best_model_state)
            if False:
                for param_tensor in model.state_dict():
                    print(param_tensor, "\t", model.state_dict()[param_tensor].size())
                print("Optimizer's state_dict:")
                for var_name in optimizer.state_dict():
                    print(var_name, "\t", optimizer.state_dict()[var_name])
        
        
        
        # Evaluate the model
        
        test_loss = 0.0
        y_true = []
        y_pred = [] 
        
        train_loss = 0.0
        y_true_test = []
        y_pred_test = []
        
        with torch.no_grad():
            model.load_state_dict(torch.load(model_save_path))
            model.eval()
            
            
            for (inputs, labels) in (test_loader):
                

                outputs = (model(inputs)) #We do this as well in case the model tries to give us a negative value...
                test_loss += criterion(outputs, labels).item() * inputs.size(0)
                y_true.extend(labels.numpy())
                y_pred.extend(outputs.numpy())
                # print(y_true)
                # print(y_pred)

            print("R2 for ", str(LS_value), ":", r2_score(y_true,y_pred))
        r2 = best_r2_score #Take the best we got.  
        with torch.no_grad():
            model.load_state_dict(torch.load(model_save_path))
            model.eval()
        


            
            for (inputs, labels) in (train_loader):
                

                outputs = (model(inputs)) #We do this as well in case the model tries to give us a negative value...
                train_loss += criterion(outputs, labels).item() * inputs.size(0)
                y_true_test.extend(labels.numpy())
                y_pred_test.extend(outputs.numpy())
                # print(y_true)
                # print(y_pred)
        with torch.no_grad():
            model.load_state_dict(torch.load(model_save_path))
            model.eval()
            outputs_leftout = model(Fleftout_tensor)
            
            Leftout_X_pred = outputs_leftout.detach().numpy()
            
        print("R2 on leftouts: ",r2_score(y_true=Lleftout_arr , y_pred=outputs_leftout))
        

        index = np.where(namese=="L/S kumuliert")[0][0]
        X_values = untransformed_test[:,index] #LS is here now.
        
        y_values =y_test_fold
        
        y_preds_training = y_pred_test #Man the naming convention here is so bad
        y_values_training = y_pred_test
        def wmape(actual, pred):
            actual = np.array(actual)
            pred = np.array(pred)
            wMAPE = np.sum(np.abs(actual-pred))/np.sum(np.abs(actual))*100
            return wMAPE
                
        def Relative_loss(actual, pred):
            norm = len(actual)
            actual = np.array(actual)
            pred = np.array(pred)
            
            # Exclude zeros from actual values
            non_zero_mask = actual != 0
            actual_non_zero = actual[non_zero_mask]
            pred_non_zero = pred[non_zero_mask]
            
            # Calculate relative loss
            relative_loss = np.sum(np.abs(actual_non_zero - pred_non_zero) / np.abs(actual_non_zero)) / norm
            return relative_loss
        
        if folder_counter == 1:
            model.load_state_dict(torch.load(model_save_path))
            model.eval()
            best_model_state = model.state_dict()
            try:
                torch.save(best_model_state, model_save_path)
            except Exception as e:
                print("Error while saving after finished training...", e)
                
            batch = next(iter(test_loader)) #batch is inputs, labels
            inputs,_ = batch #16x18 (16 per batch, 18 features)
            # print(inputs)
            
            background = inputs[:4]
            foreground = inputs[4:]
            print(f"Background shape: {background.shape}")
            print(f"Foreground shape: {foreground.shape}")

            
            explainer = shap.DeepExplainer(model, background)
            # print("okkk")
            # Compute SHAP values
            shap_values = explainer.shap_values(foreground)
            # print(shap_values)
            # print(np.shape(shap_values))
            # Visualize SHAP values
            shap.summary_plot(shap_values, foreground.detach().numpy(), feature_names=namese,show=False)
            colors = plt.cm.viridis(np.linspace(0, 1, len(X_values)))
            if LS_value is not None:
                plt.title(r"$\bf{Neural\ Network}$"+"\nElement: {} , R2 value: {:.2f}, Seed: {}".format(name, r2,seed))
            else:
                plt.title(r"$\bf{Neural\ Network}$"+"\nElement: {} , R2 value: {:.2f}, Seed: {}".format(name, r2,seed))
            if save_plot:
                plt.savefig(os.path.join(results_folder, f"shap_summary_plot_NN_{name}_{Leftout_name}.png"))
            plt.show()
                
            plt.figure()
            for i, (x_val, y_val) in enumerate(zip(X_values, y_values)):
                plt.scatter(x_val, y_val, label="True values" if i == 0 else "", marker='s', color=colors[i])
            
            # Plot predictions
            for i, (x_val, y_prd) in enumerate(zip(X_values, y_pred)):
                plt.scatter(x_val, y_prd, label = "Predictions"if i == 0 else "", marker='x', color=colors[i])
            if LS_value is not None:
                plt.title(r"$\bf{Neural\ Network}$"+"\nElement: {} , R2 value: {:.2f}, Seed: {}".format(name, r2,seed))
            else:
                plt.title(r"$\bf{Neural\ Network}$"+"\nElement: {} , R2 value: {:.2f}, Seed: {}".format(name, r2,seed))
            plt.grid(True)
            plt.xlabel("LS")
            plt.ylabel(r"Concentration  [mg/L]")
            plt.legend()
            plt.show()
            
            plt.figure()
            plt.title(r"$\bf{Neural\ Network}$"+"\nElement: {} , R2 value: {:.2f}, Seed: {}".format(name, r2,seed))
            for i, (y_val, y_prd) in enumerate(zip(y_values, y_pred)):
                plt.scatter(y_val, y_prd, marker='x', color=colors[i])
            plt.plot([min(y_values), max(y_values)], [min(y_values), max(y_values)], color='black', linestyle='--', linewidth=2)
            plt.ylabel("Predicted values")
            plt.xlabel("Actual values")
            plt.grid(True)
            plt.legend()
            if save_plot:
                plt.savefig(os.path.join(results_folder, f"Results_NN_{name}_{Leftout_name}.png"))
            plt.show()
            
            plt.title(r"$\bf{Neural\ Network}$"+"\nElement: {} , R2 value: {:.2f}, Seed: {}".format(name, r2,seed))

            plt.scatter(y_values, y_pred, marker='x', color='blue', label = "Test Data")
            plt.scatter(y_values_training,y_preds_training, marker='s', color='red', label='Training Data',alpha=0.3)
            plt.scatter(Lleftout_arr,outputs_leftout, color = 'black', label = "Leftout")
            plt.plot([min(y_values), max(y_values)], [min(y_values), max(y_values)], color='black', linestyle='--', linewidth=2)
            # core(y_true=Lleftout_arr , y_pred=outputs_leftout))
            plt.ylabel("Predicted values")
            plt.xlabel("Actual values")
            plt.grid(True)
            plt.legend()
            plt.show()
            
            plt.figure()
            plt.title(r"$\bf{Neural\ Network (Fold)}$"+"\nElement: {} , R2 value: {:.2f}, Seed: {}".format(name, r2,seed))
            plt.scatter(list_of_epochs,epoch_losses,label="Training loss on fold",s=10)
            plt.scatter(list_of_epochs,epoch_losses_test, label = "Test loss on fold",s=10)
            plt.xlabel("Epochs")
            plt.ylabel("Loss")
            plt.grid(True)
            plt.legend()
            plt.show()
            
            pred_leftout = Leftout_X_pred 
            y_pred_train = y_pred_test
            Relative_loss_for_plot = Relative_loss(Lleftout_arr, pred_leftout)
            plt.title(r"$\bf{Neural\ Network}$"+"\nElement: {} , Relative Leftout: {:.2f}, Seed: {}".format(name, Relative_loss_for_plot,seed))
            if (Leftout_name) is not None:
                plt.title(r"$\bf{Neural\ Network}$"+"\nElement: {} , Relative Leftout: {:.2f}, Seed: {},{}".format(name, Relative_loss_for_plot,seed,Leftout_name))
            for i, (y_val, y_prd) in enumerate(zip(y_values, y_pred)):
                # plt.scatter(y_val, y_prd, label = "Plot"if i == 0 else "", marker='x', color=colors[i])
                plt.scatter(y_val, y_prd, label = "Validation"if i == 0 else "", marker='x', color='green')
            # print(np.shape(y_pred_train))
            # print(np.shape(y_true_test))
            # print(np.shape(y_pred))
            # print(np.shape(y_pred_test))
            # print(np.shape(y_values))
            plt.scatter(y_true_test,y_pred_train, label = "Train", color = 'blue', marker = '.')
            plt.scatter(Lleftout_arr,pred_leftout,label = "Test",color = 'red',marker = 'D' )
            plt.plot([min(y_values), max(y_values)], [min(y_values), max(y_values)], color='black', linestyle='--', linewidth=2)
            plt.ylabel("Predicted values")
            plt.xlabel("Actual values")
            plt.grid(True)
            plt.legend()
            plt.show()
        

        
        pred_leftout = outputs_leftout
        
        RMSE_leave_out.append(mean_squared_error(Lleftout_arr, pred_leftout))
        wmap_leave_out.append(wmape(Lleftout_arr, pred_leftout))
        Relative_score_leave_out.append(Relative_loss(Lleftout_arr, pred_leftout))
        r2_score_leave_out.append(r2_score(Lleftout_arr, pred_leftout))
        # Calculate the R-squared score for the fold
        # r2 = r2_score(y_true, y_pred)
        r2 = best_r2_score #Take the best we got.
        # print("LEFTOUTS: RMSE,WMAP,REL,R2")
        # print(np.mean(RMSE_leave_out), np.mean(wmap_leave_out),np.mean(Relative_score_leave_out),np.mean(r2_score_leave_out))
        # print("Leftouts preds: ",outputs_leftout)
        # print("Leftout true: ",Lleftout_arr)
        
        r2_scores.append(r2)
        
        romana_loss = wmape(y_test_fold,y_pred)
        wmap_score.append(romana_loss)
        Relative_score.append(Relative_loss(y_test_fold,y_pred))

        r2_test = r2_score(y_true_test,y_pred_test)
        r2_scores_train.append(r2_test)
        wmap_score_train.append(wmape(y_true_test,y_pred_test))
        Relative_score_train.append(Relative_loss(y_true_test,y_pred_test)) #Yes, the names for y_true_test etc; are SUPER confusing.
        

        RMSE_train.append(mean_squared_error(y_true_test,y_pred_test))
        RMSE_test.append(mean_squared_error(y_test_fold,y_pred))
    # r2_scores = []
    # wmap_score=[]
    # Relative_score = []
    
    # r2_scores_train = []
    # wmap_score_train = []
    # Relative_score_train = []
        # print(f"Fold R-squared: {r2:.4f}")
        from sklearn.metrics import mean_absolute_percentage_error
        MSE_loss = mean_absolute_percentage_error(y_true, y_pred)
        # print("Variance: ", np.var(y_true))
        # print(RMSE_train, RMSE_test)
        # print(wmap_score,wmap_score_train)
        # print(Relative_score,Relative_score_train)
    # Calculate the average R-squared score across all folds
    avg_r2_score = np.mean(r2_scores)
    # print("Average R-squared score:", avg_r2_score)
    # Arch = Net.get_architecture()
    Arch = "Wrong place" #We dont bother with the architecture if we try to run the code. It has already been extracted.
    return avg_r2_score,np.mean(wmap_score),np.mean(Relative_score),np.mean(RMSE_test), np.mean(r2_scores_train), np.mean(wmap_score_train), np.mean(Relative_score_train),np.mean(RMSE_train),np.mean(RMSE_leave_out), np.mean(wmap_leave_out),np.mean(Relative_score_leave_out),np.mean(r2_score_leave_out),Arch


#Calcium,Zinc is very fold dependent.
if __name__ == "__main__":
    some_list = ["pH","Leitfahigkeit","L/S kumuliert","Calcium","Arsenic", "Lead (Pb+2)","Copper","Total Organic C (TOC)",
                 "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"] #Missing: Molybdan
    
    some_list_2 = ["Calcium","Arsenic", "Lead (Pb+2)","Copper","Total Organic C (TOC)",
                 "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"] 
    
    
    #add noise: adds noisy data. Adding noise in "Machine_Learning" will only add the noise when we 
    import warnings
    warnings.filterwarnings("ignore")
    
    
    if __name__ == "__main__":
        import csv
        import warnings
        import os
        import sys
        from tqdm import tqdm
        from Results_by_experiment import Sort_by_experiment
        # Suppress warnings
        warnings.filterwarnings("ignore")
        # sys.path.append(r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Preprocessing')
        # Define parameters
        reduced_in = False
        Ramona_norm = False
        LS02 = False
        meth = "Steady" #hehe.meth.
        kfold = 5
        seed_list = [42,33,11,5,2]
        # seed = 42
        # seed = seed_list[0]

        # Create a directory for results if it doesn't exist
        
        
        
        # Features, Labels,_,names = Make_Features_and_Labels(some_list, method="Steady",add_noise=True,std_width = 0.1,factor=10,add_extra = False,Boolnorm = Ramona_norm,LS02= LS02) #method = Rolling, Steady'
        Experiment_names = ["Nasschlacke 1","Nasschlacke 2","Nasschlacke 3","Nasschlacke 4","Nasschlacke 5",
                                "Nasschlacke 6","Nasschlacke 7","Nasschlacke 8","Nasschlacke 9",
                                "Mischung 1","Mischung 2","Mischung 3","Mischung 4","Mischung 5",
                                "Trockenschlacke 1","Trockenschlacke 2","Trockenschlacke 3","Trockenschlacke 4","Trockenschlacke 5",
                                "Trockenschlacke 6","Trockenschlacke 7","Trockenschlacke 8", 
                                "Magnetische Schlacke"]
        for Ename in Experiment_names:

            First_three = True 
            First_two = False #This has not yet been correctly implemented in Machine_Learning!
            feature_engineering=True
            LS_value = None
            # leave_out = Experiment_names[1]
            leave_out = Ename
            noise_bool = False
            
            pick_inputs = None#["Sulfate","L/S kumuliert","Leitfahigkeit", "pH ","Calcium","Zinc","Antimony","Total Organic C (TOC)"]#["Calcium","L/S kumuliert" ]#["Sulfate","L/S kumuliert","Leitfahigkeit", "pH ","Calcium","Zinc","Antimony","Total Organic C (TOC)"] #List of strings of the elements to be used as input.
            neurons=[[120],[60,60],[45,45,45],
                        [60,60,40,60],[40,40,40,40,40],[10,10,10,10,10,10],
                        [50,50,50,50,50,50,50],[10,20,30,40,30,20,10,5],
                        [50,40,50,40,50,40,50,40,50]]
            layers=[1,2,3,4,5,6,7,8,9]
            neurons = []
            layers=[]
            for size in range(1,6):
                for count in range(20,121,20):
                    layer=[count]*size
                    neurons.append(layer)
            for neu in neurons:
                layers.append(len(neu))
            
            current_dir=os.path.dirname(os.path.abspath(__file__))
            
            Feats, Labs,_,names, Feats_leftout,Labels_leftout = Make_Features_and_Labels(some_list, method="Steady",add_noise=noise_bool,std_width = 0.3,factor=5,add_extra = False,LS02 = False,leave_out=leave_out, First_three=First_three, First_two = First_two )

            Features = Feats
            Labels = Labs
            for n_index,neuron in enumerate(neurons):
                for l_index,layer in enumerate(layers):
                    if l_index != n_index:
                        continue
                    print(neuron,layer)
                    results_folder=os.path.join(current_dir, "LayerNeuronResults","LayerAndNeuron","layerneuron2",str(leave_out),"Checking_for_4)_robust__neuron_"+str(n_index)+"_layer_"+str(l_index)+"_onefold","LS_value"+str(LS_value))
                    for elems in some_list_2:
                        if elems != "Calcium":
                            continue
                        name = 'NN_A_Results_1'+'_Fist_Three_'+str(First_three)+'_First_two_'+str(First_two)+'_' + str(elems)+'_'+str(leave_out) + '.csv'
                        counter = 0
                        Element = str(elems)
                
                        
                        
                        if not os.path.exists(results_folder):
                            os.makedirs(results_folder)
                        
                        # Define the file path within the results folder
                        file_path = os.path.join(results_folder, name)
                        
                        with open(file_path,'a',newline='') as csvfile:
                            writer = csv.writer(csvfile)
                            header = ["Reduced input", "Ramona norm", "method", "Kernel", "K fold","Seed"]
                            for elem in some_list_2:
                                if elem == Element:
                                # if True:
                                # if elem in ["Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"]:
                                    header.extend([elem + "R2", elem + "wmap", elem + "Relative",elem + "RMSE", elem + "R2 train", elem + "wmap train", elem +"Rel train",elem + "RMSE Train",elem + "RMSE Leave", elem + "WMAP Leave", elem + "Rel Leave", elem + "R2 Leave"])
                            writer.writerow(header)
                        
                        
                            for seed in seed_list: 
                                if seed != 42:
                                    continue
                                random.seed(seed)
                                np.random.seed(seed)
                                torch.manual_seed(seed)
                                torch.cuda.manual_seed(seed)
                                torch.backends.cudnn.deterministic = True
                                np.random.seed(seed)
                                
                                print("Seed percentage: "+str(100*counter/len(seed_list)))
                                counter+=1
                
                                # Open CSV file in append mode
                                # Architecture = Net.get_architecture()
                                Architecture = "See own file"
                                row_data = [reduced_in, Ramona_norm, meth, Architecture, kfold, seed]
                                # Loop through elements in some_list_3
                                for elem in tqdm(some_list_2, desc = "Elements", position=0, leave=True):
                                    if elem == Element:# 
                                        # print("Faen i helvete asså")
                                        print(neuron,layer,seed,Ename,elem)
                                    # if elem in ["Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"]:
                                    # if True:
                                        _,_,_,_,_,_,_,_,_,Architecture = Machine_Learning(Features, Labels,Feats_leftout,Labels_leftout, names, kfold,seed, neurons = neuron, layers = layer,std_width=0.1, add_noise=False, factor=5, name=str("Calcium"), reduced_input=reduced_in,Archit = True,LS_value=None,pick_inputs=pick_inputs)
                                        r2s = []
                                        txt_file_path = os.path.join(results_folder, str(elem)+"_Architecture")
                                        Architecture.append(pick_inputs)
                                        np.savetxt( txt_file_path, (Architecture),fmt='%s')
                                        print("Saved")
                                        
                                        r2, wmap, rel,RMSE, r2_train,wmap_train, Rel_train,RMSE_train, RMSE_l,WMAP_l,Rel_l,R2_l,_ = Machine_Learning(Features, Labels,Feats_leftout,Labels_leftout, names, kfold,seed, neurons=neuron, layers=layer,std_width=0.1, add_noise=False, factor=2, 
                                                                                                                                                     name=str(elem), reduced_input=reduced_in,save_plot=True,Leftout_name=str(leave_out),results_folder=results_folder,LS_value=LS_value,pick_inputs=pick_inputs,
                                                                                                                                                     feature_engineering=feature_engineering,first_two=First_two,first_three=First_three)
                                        r2s.append(r2)
                      
                                        row_data.append(r2)
                                        row_data.append(wmap)
                                        row_data.append(rel)
                                        row_data.append(RMSE)
                                        row_data.append(r2_train)
                                        row_data.append(wmap_train) 
                                        row_data.append(Rel_train)
                                        row_data.append(RMSE_train)
                                        row_data.append(RMSE_l)
                                        row_data.append(WMAP_l)
                                        row_data.append(Rel_l)
                                        row_data.append(R2_l)
                                        print(r2, wmap, rel,RMSE, r2_train,wmap_train, Rel_train,RMSE_train, RMSE_l,WMAP_l,Rel_l,R2_l)
                                    # writer.writerow(row_data)
                                print("The r2 values were:"+str(r2s))
                                    # Write row to CSV file
                                writer.writerow(row_data)
                                csvfile.flush()
                                    # Append element-specific data to the row
                                # row_data.extend([elem, MSE, r2])
                        
    
    
    
