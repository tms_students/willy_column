import numpy as np
import torch
import random
import sys
import re
import os
sys.path.append(r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Preprocessing')
# Your existing code
from Last_Preprocess_Overall import Make_Features_and_Labels
import torch.nn as nn
from sklearn.model_selection import KFold
import torch.optim as optim
from torch.utils.data import DataLoader, TensorDataset
import torch.nn.functional as F
from sklearn.metrics import r2_score,mean_squared_error
from sklearn.preprocessing import MinMaxScaler,StandardScaler, RobustScaler,FunctionTransformer
import difflib
from sklearn.datasets import load_iris #Just to check
import inspect
import shap
import pandas as pd
# sys.path.append(r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Preprocessing')
import matplotlib.pyplot as plt
import math

from sklearn.metrics import mean_squared_error,mean_absolute_percentage_error

def Plot_errorbars(actual,predicted,Element):
    arr_a = actual
    arr_p = predicted
    medians_a = np.median(arr_a, axis=0)
    std_devs_a = np.std(arr_a, axis=0)
    
    Number_of_Experiments, Number_of_LS_vals = np.shape(actual)
    medians_p = np.median(arr_p, axis=0)
    std_devs_p = np.std(arr_p, axis=0)
    
    LSNames = ['1','2','5','10']
    LSNames = LSNames[:Number_of_LS_vals]
    
    plt.subplot(1, 2, 1)
    plt.boxplot(arr_a, labels=[x for x in LSNames])
    plt.xlabel('Columns')
    plt.ylabel('Values')
    plt.title('Box Plot of arr_a')
    plt.grid(True)
    
    plt.subplot(1, 2, 2)
    plt.boxplot(arr_p, labels=[x for x in LSNames])
    plt.xlabel('Columns')
    plt.ylabel('Values')
    plt.title('Box Plot of arr_p')
    plt.grid(True)
    
    plt.tight_layout()
    plt.show()
    
    # Plot the median and standard deviation
    x = np.arange(1, arr_a.shape[1] + 1)
    
    plt.figure(figsize=(12, 6))
    
    plt.errorbar(x, medians_a, yerr=std_devs_a, fmt='o', label='Actual')
    plt.errorbar(x, medians_p, yerr=std_devs_p, fmt='o', label='Predicted')
    
    plt.xticks(x, [x for x in LSNames])
    plt.xlabel('LS Values')
    plt.ylabel('Concentrations')
    plt.title('Median and Standard Deviation of '+str(Element))
    plt.legend()
    plt.grid(True)
    plt.tight_layout()
    plt.show()
    return None

def Evaluate_Data(Features, Labels,Feats_leftout, Labels_leftout,names,factor,neurons=[120],Layers=1,path="",std_width = 0.1,name = "Copper (Cu)",
                  reduced_input = False,Archit = False,LS_value=None, name_leftout = None,Ename=None,model="Neural",pick_inputs=None,feature_engineering=False,first_three = False,first_two=False,plot=True):
    
        # Define the model
    
    print("#neurons",neurons)
    print("#layers", Layers)

    shape_1 = np.shape(Features) 
    shape_2 = np.shape(Labels)
    Feature_Matrix_ = np.array(Features)
    Label_Vector_ = np.array(Labels)
    
    Feature_Matrix_leftout = np.array(Feats_leftout)
    Label_Vector_leftout = np.array(Labels_leftout)

    if name_leftout is None:
        print("was onen")
        Feature_Matrix_leftout = Feature_Matrix_
        Label_Vector_leftout = Label_Vector_


    Length = int(shape_1[0]/(factor+1))#This is the length of the real, not generated data.
    if factor == 0:
        Length = int(shape_1[0])
    
    
    
    
    if LS_value is not None: #Remove the training for small LS values.
        if isinstance(LS_value, list):
            mask = ~np.isin(Label_Vector_[:,2], LS_value)  # Inverse the condition
            Leftmask = ~np.isin(Label_Vector_leftout[:,2], LS_value)
        else:
            mask = (Label_Vector_[:,2] == LS_value)  # Inverse the condition
            Leftmask = (Label_Vector_leftout[:,2] == LS_value) 

        # mask = (Label_Vector_[:,2] != LS_value)
        Feature_Matrix_ = Feature_Matrix_[mask]
        Label_Vector_ = Label_Vector_[mask]
        
        Label_Vector_leftout = Label_Vector_leftout[Leftmask]
        Feature_Matrix_leftout = Feature_Matrix_leftout[Leftmask]
        
    

    folder_counter = 1
    
    # print(np.shape(Feature_Matrix_))
    Feats=Feature_Matrix_

    epsilon = 1.123e-4 #1e-7 is much smaller than anything else in the data set
    scaler = FunctionTransformer(lambda x: np.log(np.abs(x + epsilon)), validate=True)
    # scaler = MinMaxScaler() #C = (c_i - c_min)/(c_max - c_min) -> Range in [0,1]
    # scaler=StandardScaler()
    scaler=RobustScaler()

    
    

    combined_features = np.vstack((Feats, Feature_Matrix_leftout))
    scaled_combined_features = scaler.fit_transform(combined_features) 
    Feats = scaled_combined_features[:Feats.shape[0]]
    Feature_Matrix_leftout = scaled_combined_features[Feats.shape[0]:]
    

    # print(Feats[:,2])
    # print(np.shape(Feats))
    if len(name)!=0:
        close_match = difflib.get_close_matches(name + "LS01", names, n=1,cutoff = 0.6)
        if len(close_match) == 0:
            close_match = difflib.get_close_matches(name, names, n=1,cutoff = 0.6)#-3 since first three are removed from labels, and names has len(features)
        index = (np.where(np.array(names) == close_match)[0][0])-3 #-3 since first three are removed from labels, and names has len(features)
    
        
        # Feats = Feature_Matrix_[:,index+3]
        # Feats = Feats.reshape(-1,1)
        
        Labs = Label_Vector_[:,index+3]
        Labs = Labs.reshape(-1,1)

        Label_Vector_leftout = Label_Vector_leftout[:,index+3]
        Label_Vector_leftout = Label_Vector_leftout.reshape(-1,1)
    
        shape_2 = np.shape(Labs)
        
        if reduced_input: #This will make only LS, Leitf, LS*elem, plus element of output to input.
            # X_train_fold = np.concatenate((X_train_fold[:, :3], X_train_fold[:, index+3].reshape(-1, 1)), axis=1)
            Feats = np.concatenate((Feats[:, :3], Feats[:, index+3].reshape(-1, 1), (Feats[:, 2] * Feats[:, index+3]).reshape(-1, 1)), axis=1)
            # X_train_fold = X_train_fold.reshape(-1,1)
            Feature_Matrix_leftout = np.concatenate((Feature_Matrix_leftout[:, :3], Feature_Matrix_leftout[:, index+3].reshape(-1, 1), (Feature_Matrix_leftout[:, 2] * Feature_Matrix_leftout[:, index+3]).reshape(-1, 1)), axis=1)
            shape_1 = np.shape(Feats)
            

            string_to_add = "LS*"+str(name)
            string_series = pd.Series([string_to_add])
            a_string_series = pd.Series([str(name)])
            namese = pd.concat([names[:3], a_string_series,string_series])
            namese=namese.reset_index(drop=True)
        else:
            if feature_engineering:
                Feats = np.concatenate((Feats, (Feats[:, 2] * Feats[:, index+3]).reshape(-1, 1).reshape(-1, 1),(1/Feats[:, 2] * Feats[:, index+3]).reshape(-1, 1),(np.exp(-Feats[:, 2]) * Feats[:, index+3]).reshape(-1, 1)), axis=1)
                
                Feature_Matrix_leftout = np.concatenate((Feature_Matrix_leftout, (Feature_Matrix_leftout[:, 2] * Feature_Matrix_leftout[:, index+3]).reshape(-1, 1).reshape(-1, 1),(1/Feature_Matrix_leftout[:, 2] * Feature_Matrix_leftout[:, index+3]).reshape(-1, 1),(np.exp(-Feature_Matrix_leftout[:, 2]) * Feature_Matrix_leftout[:, index+3]).reshape(-1, 1)), axis=1)
                string_to_add = "LS*"+str(name)
                string_to_add_2 = "LS/"+str(name)
                string_to_add_3 = "exp(-"+str(name) + ')'
                string_series = pd.Series([string_to_add,string_to_add_2,string_to_add_3])
                namese = pd.concat([names, string_series])
                namese=namese.reset_index(drop=True)
            if feature_engineering == False:
                # Feats = np.concatenate((Feats, (Feats[:, 2] * Feats[:, index+3]).reshape(-1, 1).reshape(-1, 1)), axis=1)
                # Feature_Matrix_leftout = np.concatenate((Feature_Matrix_leftout, (Feature_Matrix_leftout[:, 2] * Feature_Matrix_leftout[:, index+3]).reshape(-1, 1).reshape(-1, 1)), axis=1)
                # shape_1 = np.shape(Feats)
                
                    
                # string_to_add = "LS*"+str(name)
                # string_series = pd.Series([string_to_add])
                namese = pd.concat([names])
                namese=namese.reset_index(drop=True)
            
        nameinput = name
        if pick_inputs is not None:
            def get_closest_indices(names, names_to_keep):
                closest_indices = []
                for name in names_to_keep:
                    
                    closest_match1 = difflib.get_close_matches(name, names, n=1, cutoff=1) #Exact if L/S Kumuliert, LS*Calcium, pH , etc;
                    if closest_match1:
                        closest_indices.append(np.where(names == closest_match1[0])[0][0])
                    else:
                        # Find the closest base name in the names array (ignoring LSX)
                        closest_match = difflib.get_close_matches(name, names, n=3, cutoff=0.4)
                        if name == nameinput:
                            closest_match = difflib.get_close_matches(name, names, n=4, cutoff=0.4) #Because also LS*element exists. So there are 4.
                            if feature_engineering:
                                closest_match = difflib.get_close_matches(name, names, n=6, cutoff=0.5)
                        # print("Name:", name)
                        # print("Base Names:", base_names)
                        
                        if closest_match:
                            for matches in closest_match:
                    
                                closest_indices.append(np.where(names == matches)[0][0])
                                

                    if first_three == False and first_two==False:
                        closest_indices = []
                        for name in names_to_keep:
                            closest_match1 = difflib.get_close_matches(name, names, n=1, cutoff=1) #Exact if L/S Kumuliert, LS*Calcium, pH , etc;
                            if closest_match1:
                                closest_indices.append(np.where(names == closest_match1[0])[0][0])
                            else:
                                closest_match = difflib.get_close_matches(name, names, n=3, cutoff=0.7)
                                if closest_match:
                                    for matches in closest_match:
                                        closest_indices.append(np.where(names == matches)[0][0])
                                
                return closest_indices, pick_inputs
            
            indices_to_use, _ = get_closest_indices(namese, pick_inputs)
            namese = namese[indices_to_use].reset_index(drop=True)
            if folder_counter==1:
                print("We will use: ",namese, "as our input")
            Feature_Matrix_leftout = Feature_Matrix_leftout[:,indices_to_use]
            Feats=Feats[:,indices_to_use]
            shape_2 = np.shape(Labs)
            shape_1 = np.shape(Feats)
    # print("We are using:",namese, "as our input")
    shape_1 = np.shape(Feats) 
    shape_2 = np.shape(Labs)

    
    Scaled_X = (Feats)
    Tensor_X = torch.tensor(Scaled_X, dtype=torch.float32)
    Tensor_y = torch.tensor(Labs, dtype=torch.float32)
    
    Scaled_X_leftout = (Feature_Matrix_leftout)
    Tensor_X_leftout = torch.tensor(Scaled_X_leftout, dtype=torch.float32)
    Tensor_y_leftout = torch.tensor(Label_Vector_leftout, dtype=torch.float32)
                
    class NegELU(nn.Module):
        def __init__(self):
            super(NegELU, self).__init__()
            self.elu = nn.ELU()
        
        def forward(self, x):
            return self.elu(-x)
    
    # Define the model
    class Net(nn.Module):
        """
        The model class, which defines our feature extractor used in pretraining.
        """
    
        def __init__(self, neurons, layers):
            """
            The constructor of the model.
            """
            super(Net,self).__init__()
            # Define the architecture of the model.
            self.layers = nn.Sequential()
            self.layers.add_module("Starting layer:",nn.Linear(shape_1[1],neurons[0]))
            for i in range(1,(layers)):
                self.layers.add_module(f'hidden_layer_{i}', nn.Linear(neurons[i-1], neurons[i]))
                self.layers.add_module(f'hidden_activation_{i}', nn.LeakyReLU(0.01))
                self.layers.add_module(f'Elu_activation_{i}',NegELU())
                self.layers.add_module(f'dropout_{i}', nn.Dropout(p=0.3))
            self.layers.add_module('hidden_activation_output', nn.LeakyReLU(0.01))
            self.layers.add_module('Elu_activation_output',NegELU())
            self.layers.add_module('dropout_output', nn.Dropout(p=0.3))
            self.layers.add_module('output_layer',nn.Linear(neurons[-1],out_features=shape_2[1]))
            
            
            
            self.weight_layer = nn.Linear(shape_2[1],shape_2[1])
            self.dropout = nn.Dropout(p=0.3)
            self.leaky = nn.LeakyReLU(0.01)
    
            
            self.trainable_constant = nn.Parameter(torch.randn(1)) 
            
        def forward(self, x):
    
            x = self.layers(x)
    
            return x #+ self.trainable_constant#By extension, this should then look like Aexp(-bx) + c
    
    string_element = str(name)
    # string_seed = str(seed)
    # string_folder_counter = str(1) #Some random fold. One should actually go over all the folds and then take the average. So instead of 1, go 0 to 4, then take avg.
    
    if len(path)==0:
        
        # model_save_path = r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\C_Results\Dictionaries\ ' + string_element + '_'+string_seed + '_' + string_folder_counter + '.pt'
        directory = r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\A_Results_1_noise\Dictionaries'
        #C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Nasschlacke 1\Neural_Network_B3\Dictionaries
        #'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\A_Results_1\Dictionaries'
    else:
        directory = path 
    if 'noise' in directory:
        add_noise = True
    else:
        add_noise=False
    element_name = string_element
    
    model_saves=os.listdir(directory)
    # print(model_saves)
    pattern = re.compile(rf"{element_name}_\d+_\d+\.pt")
    if LS_Value is not None:
        pattern = re.compile(rf"{element_name}_\d+_\d+\d+\.pt")
    
    predictions_real = []
    predictions_generated = []
    predictions_leftout = []
    temp=0
    # print(element_name)
    for save in model_saves:
        if element_name in save:
            temp+=1

    print("We found: ",temp," save files")
    temp=0
    for save in model_saves:
        
        # print(save)
        if element_name in save:

            model_path = os.path.join(directory, save)
            # print(model_path)

            
            model = Net(neurons,Layers)
            model.load_state_dict(torch.load(model_path))
            model.eval()
            with torch.no_grad():
                Real_X = Tensor_X[:Length,:]
                Real_pred = model(Real_X)
                Real_Numpy_pred = Real_pred.detach().numpy()
                
                Generated_X = Tensor_X[Length:,:]
                Generated_Pred = model(Generated_X)
                Generated_Numpy_pred = Generated_Pred.detach().numpy()
                
                Leftout_X = model(Tensor_X_leftout)
                Leftout_X_pred = Leftout_X.detach().numpy()
                
                predictions_real.append(Real_Numpy_pred)
                predictions_generated.append(Generated_Numpy_pred)
                predictions_leftout.append(Leftout_X_pred)
    
    
    predictions_real = [0 if isinstance(x, float) and math.isnan(x) else x for x in predictions_real]
    predictions_generated = [0 if isinstance(x, float) and math.isnan(x) else x for x in predictions_generated]
    predictions_leftout = [0 if isinstance(x, float) and math.isnan(x) else x for x in predictions_leftout]

    Averaged_Reals = np.mean(predictions_real, axis=0) #Predictions
    
    # Averaged_Reals = predictions_real[3]
    # print(np.shape(predictions_real))
    
    Averaged_Generated = np.mean(predictions_generated,axis=0) #Predictions
    
    
    
    Averaged_leftout = np.mean(predictions_leftout,axis=0)
    
    
    
    Real_X = Tensor_X[:Length,:]
    Real_y = Tensor_y[:Length,:]
    
    Real_Numpy_X = Real_X.detach().numpy()
    Real_Numpy_y =Real_y.detach().numpy()
    
    
    Generated_X = Tensor_X[Length:,:]
    Generated_y = Tensor_y[Length:,:]
    
    Generated_Numpy_X = Generated_X.detach().numpy()
    Generated_Numpy_y = Generated_y.detach().numpy()
    
    Leftout_numpy_X_pred = Averaged_leftout
    Leftout_numpy_Y = Tensor_y_leftout.detach().numpy()
    
    
    #LS_values_Real_X = Real_Numpy_X[:,2]
    index = np.where(namese=="L/S kumuliert")[0][0]
    LS_values_Real_X = Real_Numpy_X[:,index]
    X_values = Real_Numpy_X[:,index] #LS is here now.
    
    LS_values_Generated_Numpy_X = Generated_Numpy_X[:,index]
    LS_values_leftout_numpy_X = Feature_Matrix_leftout[:,index]


    actual_l = Leftout_numpy_Y
    predicted_l = Leftout_numpy_X_pred
    actual_l = np.nan_to_num(actual_l, nan=0.0)
    predicted_l = np.nan_to_num(predicted_l, nan=0.0)
    Leftout_numpy_X_pred = np.nan_to_num(Leftout_numpy_X_pred, nan=0.0)
    Averaged_Reals = np.nan_to_num(Averaged_Reals, nan=0.0)
    
    

    try:
        R2_Leftout = r2_score(actual_l,predicted_l)
        Rel_Leftout = Relative_loss(actual_l, predicted_l)
        # print("R2 Leftouts: ",R2_Leftout)
        # print("R2 Real training data: ",r2_score(Real_Numpy_y,Averaged_Reals))
        # print("Relative Loss Leftout: ",Rel_Leftout)
        MSE_Leftout = mean_squared_error(actual_l, predicted_l)
        WMAP_Leftout = wmape(actual_l, predicted_l)
    except Exception as e:
        print(e)
    # print(LS_values_leftout_numpy_X)
    # #####################################
    # print(Averaged_Reals)
    

    
    R2_real,R2_generated,MSE_real,MSE_generated,wmap_real,wmap_generated,Rel_real,Rel_generated= (Plotting_and_errors(Real_Numpy_y,Averaged_Reals, Generated_Numpy_y,Averaged_Generated,LS_values_Real_X,LS_values_Generated_Numpy_X,predicted_l,actual_l, name_of_model = "Neural Network", element=name,Ename = Ename,add_noise=add_noise, plot = plot))

    
    # ######
    if plot:
        background = Tensor_X[:100]  # Select a small subset for background
        explainer = shap.DeepExplainer(model, background)
        shap_values = explainer.shap_values(Tensor_X)
        
        shap.summary_plot(shap_values, Tensor_X.numpy(), feature_names=namese, show=False)
        plt.title("Neural Network: "+str(name)+ " R2: "+str(np.round(R2_real,3)))
        plt.show()
    # ######
    # actual = Leftout_numpy_Y
    # predicted = Leftout_numpy_X_pred
    # R2_leftout = r2_score(actual,predicted)
    # print("The leftouts:")
    # plt.figure()
    # plt.title("Neural Network"+str(np.round(R2_leftout,2)))
    # plt.scatter(actual,predicted)
    # plt.xlabel("Actual")
    # plt.ylabel("Predictions")
    # plt.legend()
    # plt.plot([min(actual), max(actual)], [min(actual), max(actual)], color='black', linestyle='--', linewidth=2)
    # plt.show()
    # ########
    # ##########################################
    

    return R2_real,R2_generated,R2_Leftout,MSE_real,MSE_generated,MSE_Leftout,wmap_real,wmap_generated,WMAP_Leftout,Rel_real,Rel_generated,Rel_Leftout,actual_l,predicted_l
        
        
        
def Plotting_and_errors(actual, predicted, gactual,gpredicted,LS, gLS,predleftout = None, Lleftout=None, name_of_model="",element = "",Ename=None,add_noise=False,Number_of_experiemnts=None,plot=True): #Actual, predicted, generated actuals and generated predicted.
    #Define errors:
    # print(np.shape(actual))
    # print(np.shape(predicted))

    R2_real = r2_score(actual,predicted)
    if len(gactual)!= 0:
        R2_generated = r2_score(gactual,gpredicted)
        MSE_generated = mean_squared_error(gactual,gpredicted)
        Rel_generated = Relative_loss(gactual,gpredicted)
    elif len(gactual )==0:
        R2_generated,MSE_generated,Rel_generated = None, None, None
    # print(R2_generated)
    MSE_real = mean_squared_error(actual,predicted)
    
    wmap_real = wmape(actual,predicted)
    if len(gactual)!=0:
        wmap_generated = wmape(gactual,gpredicted)
    else:
        wmap_generated=None

    Rel_real = Relative_loss(actual,predicted)

    
    #Deal with laziness on user side:    
    if len(name_of_model)==0:
        name_of_model = "Model"
    if len(element)==0:
        element = "Element"
    if plot:
        
        plt.figure()
        if predleftout is not None and Ename is not None:
            RelLeftout = Relative_loss(Lleftout,predleftout)
            plt.title(r"$\bf{" + name_of_model+': '+Ename + ": {} }$" + "\nElement: {}, Relative {}".format(element,np.round(RelLeftout,2)))
        elif (predleftout) is None:
                plt.title(r"$\bf{" + name_of_model + ": {} }$" + "\nElement: {}, R2: {}".format(element,np.round(R2_real,2)))
        elif Ename is None:
            plt.title(r"$\bf{" + name_of_model + ": {} }$" + "\nElement: {}, R2: {}".format(element,np.round(R2_real,2)))
        
        if len(gactual) != 0:
            print(len(gactual))
            print(len(actual))
            plt.scatter(gactual, gpredicted, marker='s', color='green', label='Generated data',alpha=0.3)
        plt.scatter(actual, predicted, marker='x', color='blue', label='Training Data')
        plt.plot([min(actual), max(actual)], [min(actual), max(actual)], color='black', linestyle='--', linewidth=2)
        if Lleftout is not None and predleftout is not None:
            plt.scatter(Lleftout,predleftout, marker = 'd',label = "Leftout",color='red')
            # plt.scatter(gactual,gpredicted,marker='x', color='blue', label='Real data')
        plt.ylabel("Predicted values")
        plt.xlabel("Actual values")
        plt.grid(True)
        plt.legend()
        plt.show()
    

    Experiment_names = ["Nasschlacke 1","Nasschlacke 2","Nasschlacke 3","Nasschlacke 4","Nasschlacke 5",
                            "Nasschlacke 6","Nasschlacke 7","Nasschlacke 8","Nasschlacke 9",
                            "Mischung 1","Mischung 2","Mischung 3","Mischung 4","Mischung 5",
                            "Trockenschlacke 1","Trockenschlacke 2","Trockenschlacke 3","Trockenschlacke 4","Trockenschlacke 5",
                            "Trockenschlacke 6","Trockenschlacke 7","Trockenschlacke 8", 
                            "Magnetische Schlacke"]
    if plot:
        if Number_of_experiemnts == None:
            Experiment_names_without_ename = [name for name in Experiment_names if name != Ename]
        elif Number_of_experiemnts != None:
            Experiment_names_without_ename = [name for name in Experiment_names if name != Ename]
            Experiment_names_without_ename = Experiment_names_without_ename[:Number_of_experiemnts]
        
        if predleftout is not None and Ename is not None:
            RelLeftout = Relative_loss(Lleftout,predleftout)
            plt.title(r"$\bf{" + name_of_model+': '+Ename + ": {} }$" + "\nElement: {}, Relative {}".format(element,np.round(RelLeftout,2)))
        elif (predleftout) is None:
                plt.title(r"$\bf{" + name_of_model + ": {} }$" + "\nElement: {}, R2: {}".format(element,np.round(R2_real,2)))
        elif Ename is None:
            plt.title(r"$\bf{" + name_of_model + ": {} }$" + "\nElement: {}, R2: {}".format(element,np.round(R2_real,2)))
        if len(np.shape(actual))==2:
            m,n = np.shape(actual)
        elif len(np.shape(actual))==1:
            m = np.shape(actual)
        

        if Number_of_experiemnts == None:
            Number_of_experiemnts = 22
        Amount_of_colors = Number_of_experiemnts #22 Experiments
        # print(Amount_of_colors)
        if not isinstance(m, int):
            m = m[0]
        
    
        Temp_factor = int(m/Amount_of_colors)
        colors = plt.cm.viridis(np.linspace(0, 1, Amount_of_colors))
        
        for i in range(Amount_of_colors):
            
            plt.scatter(actual[(Temp_factor*i):Temp_factor*i+4], predicted[Temp_factor*i:Temp_factor*i+4], marker='x', color=colors[i], label=Experiment_names_without_ename[i])
        
        plt.plot([min(actual), max(actual)], [min(actual), max(actual)], color='black', linestyle='--', linewidth=2)
        if Lleftout is not None and predleftout is not None:
            plt.scatter(Lleftout,predleftout, marker = 'd',label = "Leftout",color='red')
            # plt.scatter(gactual,gpredicted,marker='x', color='blue', label='Real data')
        plt.ylabel("Predicted values")
        plt.xlabel("Actual values")
        plt.grid(True)
        plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=3)
        plt.show()
    # print("The Variance is (Training Data):" ,np.std(actual))
    # print("The Variance is (Lefout):",np.std(predleftout))
    
    # plt.figure()
    # plt.title(r"$\bf{" + name_of_model + ": {} }$" + "\nElement: {}, R2: {}".format(element,np.round(R2_real,2)))
    # if Ename is not None:
    #     plt.title(r"$\bf{" + name_of_model + ": {} }$" + "\nElement: {}, R2: {}, {}".format(element,np.round(R2_real,2),Ename))
    # plt.scatter(gLS, gactual, marker='s', color='red', label='Generated data',alpha=0.3)
    # plt.scatter(gLS, gpredicted,marker='s', color='orange', label='Generated data predicted',alpha=0.3)
    # plt.scatter(LS, actual, marker='x', color='blue', label='Real data')
    # plt.scatter(LS, predicted, marker='x', color='black', label='Real data predicted')

    # plt.ylabel(r"Concentrations $\mu /mg$")
    # plt.xlabel("LS-Values")
    # plt.grid(True)
    # plt.legend()
    # plt.show()
    
    return R2_real,R2_generated,MSE_real,MSE_generated,wmap_real,wmap_generated,Rel_real,Rel_generated

def wmape(actual, pred):
    actual = np.array(actual)
    pred = np.array(pred)
    wMAPE = np.sum(np.abs(actual-pred))/np.sum(np.abs(actual))*100
    return wMAPE
        
def Relative_loss(actual, pred):
    norm = len(actual)
    actual = np.array(actual)
    pred = np.array(pred)
    
    # Exclude zeros from actual values
    
    non_zero_mask = actual != 0
    actual_non_zero = actual[non_zero_mask]
    pred_non_zero = pred[non_zero_mask]
    
    # Calculate relative loss
    relative_loss = np.sum(np.abs(actual_non_zero - pred_non_zero) / np.abs(actual_non_zero)) / norm
    return relative_loss
    
    return 0
def ignore_highest_n_numbers(arr, N):
    # Check if N is greater than or equal to the length of the array
    if N >= len(arr):
        return np.array([])  # Return an empty array
    
    # Sort the array
    sorted_arr = np.sort(arr)
    
    # Ignore the highest N numbers
    result = sorted_arr[:-N]
    
    return result

        
if __name__ == "__main__":
    some_list = ["pH","Leitfahigkeit","L/S kumuliert","Calcium","Arsenic", "Lead (Pb+2)","Copper","Total Organic C (TOC)",
                 "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"] #Missing: Molybdan
    
    some_list_2 = ["Calcium","Arsenic", "Lead (Pb+2)","Copper","Total Organic C (TOC)",
                 "Manganese","Nickel","Zinc","Cobalt", "Aluminium","Chromium","Antimony","Chloride","Sulfate","Cadmium"] 
    
    
    #add noise: adds noisy data. Adding noise in "Machine_Learning" will only add the noise when we 
    import warnings
    warnings.filterwarnings("ignore")
    
    
    if __name__ == "__main__":
        import csv
        import warnings
        import os
        import sys
        from tqdm import tqdm
        from Results_by_experiment import Sort_by_experiment
        torch.backends.cudnn.deterministic = True
        # Suppress warnings
        warnings.filterwarnings("ignore")
        # sys.path.append(r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Preprocessing')
        # Define parameters
        reduced_in = False
        Ramona_norm = False
        LS02 = False
        feature_engineering=True
        First_three = True
        First_two = False#First two has not been implemented (yet)
        meth = "Steady" #hehe.meth.
        kfold = 5
        factor = 0
        counter = 0
        Element = "Calcium"
        LS_values = ['1','2','5','10']
        
        #["Calcium","L/S kumuliert" ]
        pick_inputs = None#["Sulfate","L/S kumuliert","Leitfahigkeit", "pH ","Calcium","Zinc","Antimony","Total Organic C (TOC)"]#["Calcium","L/S kumuliert" ]#None#["Sulfate","L/S kumuliert","Leitfahigkeit", "pH ","Calcium","Zinc","Antimony","Total Organic C (TOC)"]
        # pick_inputs=None
        
        means_list = []
        R2_list = []
        neurons = []
        layers=[]
        noises = [0,0.1, 0.2,0.3,0.4,0.5,1,2]
        for size in range(1,3):
            for count in range(80,150,5):
                if size > 1 and count > 100:
                    layer=[int(count/2)]*size
                    neurons.append(layer)
                    continue
                layer=[count]*size
                neurons.append(layer)
        for neu in neurons:
            layers.append(len(neu))
            
        for noise in noises:
            for n_index,neuron in enumerate(neurons):
                for l_index,layer in enumerate(layers):
                    if n_index != l_index:
                        continue
    
                    
                    for Element in some_list_2:
                        if Element != "Calcium":
                            continue
                        print(Element)
                        Experiment_names = ["Nasschlacke 1","Nasschlacke 2","Nasschlacke 3","Nasschlacke 4","Nasschlacke 5",
                                                "Nasschlacke 6","Nasschlacke 7","Nasschlacke 8","Nasschlacke 9",
                                                "Mischung 1","Mischung 2","Mischung 3","Mischung 4","Mischung 5",
                                                "Trockenschlacke 1","Trockenschlacke 2","Trockenschlacke 3","Trockenschlacke 4","Trockenschlacke 5",
                                                "Trockenschlacke 6","Trockenschlacke 7","Trockenschlacke 8", 
                                                "Magnetische Schlacke"]
                        add_noise=False
                        save = False
                        All = True
                        LS_Value = None
                        plot = False #Creates plots for each experiments (with trianing and validation etc;)
                        results_relative_for_print = []
                        Names_for_plot = []
                        actual_ls = []
                        predicted_ls = []
                        results = []
                        for Ename in Experiment_names:
                            Experiment_run_name = 'Neural_Network_C\LS_valueNone'
                            # Experiment_run_name = 'Neural_Network_D_deeper\LS_valueNone'
                            Experiment_run_name = 'Pick_Results_1_NN\LS_valueNone'
                            Experiment_run_name = "Checking_for_5)_onefold\LS_valueNone"
                            Experiment_run_name = "Checking_for_4)_standard_onefold\LS_valueNone"
                            Experiment_run_name="Checking_for_4)_robust__neuron_"+str(n_index)+"_layer_"+str(l_index)+"_onefoldNoise_None"
                            
                            if Ename in ["Nasschlacke 1","Nasschlacke 2","Nasschlacke 3","Nasschlacke 4","Nasschlacke 5",
                                                    "Nasschlacke 6","Nasschlacke 7","Nasschlacke 8","Nasschlacke 9",
                                                    "Mischung 1","Mischung 2","Mischung 3","Mischung 4","Mischung 5",
                                                    "Trockenschlacke 1","Trockenschlacke 2","Trockenschlacke 3","Trockenschlacke 4","Trockenschlacke 5",
                                                    "Trockenschlacke 6","Trockenschlacke 7","Trockenschlacke 8",
                                                    "Magnetische Schlacke"]:
                            # if Ename in ["Nasschlacke 1","Nasschlacke 2","Nasschlacke 4","Nasschlacke 6","Nasschlacke 7","Nasschlacke 8","Nasschlacke 9",
                            #              "Mischung 2","Mischung 3","Mischung 4","Mischung 5","Trockenschlacke 1","Trockenschlacke 2","Trockenschlacke 3",
                            #              "Trockenschlacke 5", "Trockenschlacke 6","Trockenschlacke 7","Trockenschlacke 8","Magnetische Schlacke"]:
                                print(Ename)
                                base_path = r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column'
    
                                path = os.path.join(base_path, 'LayerNeuronResults', 'LayerAndNeuron','layerneuron4_noises', Ename,Experiment_run_name,"LS_value"+str(LS_Value),'Dictionaries')
                                
    
                                if LS_Value is not None:
                                    path = os.path.join(base_path, Ename, Experiment_run_name, 'LS_value'+str(LS_Value),'Dictionaries')
                                
                                #path = r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Nasschlacke 1\Neural_Network_B3\Dictionaries'
                                Features, Labels,_,names,Feats_leftout,Labels_leftout = Make_Features_and_Labels(some_list, method="Steady",add_noise=add_noise,std_width = 0.1,factor=factor,add_extra = False,Boolnorm = Ramona_norm,LS02= LS02, leave_out =Ename, First_three=First_three, First_two=First_two) #method = Rolling, Steady'
                                
            
                                R2_real,R2_generated,R2_Leftout,MSE_real,MSE_generated,MSE_Leftout,wmap_real,wmap_generated,WMAP_Leftout,Rel_real,Rel_generated,Rel_Leftout,actual_l,pred_l = (Evaluate_Data(Features, Labels,Feats_leftout,Labels_leftout,names,neurons=neuron,Layers=layer,
                                                                                                                                                                                                             path=path,factor=factor,std_width = 0.1,name = str(Element), reduced_input = False,Archit = False,LS_value=LS_Value,name_leftout = Ename,Ename = Ename,pick_inputs=pick_inputs,
                                                                                                                                                                                                             feature_engineering=feature_engineering, first_three=First_three, first_two = First_two,plot=plot))
                                #print(R2_real,R2_generated,R2_Leftout,MSE_real,MSE_generated,MSE_Leftout,wmap_real,wmap_generated,WMAP_Leftout,Rel_real,Rel_generated,Rel_Leftout)
                                results.append([Ename, R2_real, R2_generated, R2_Leftout, MSE_real, MSE_generated, MSE_Leftout, wmap_real, wmap_generated, WMAP_Leftout, Rel_real, Rel_generated, Rel_Leftout])
                                results_relative_for_print.append(Rel_Leftout)
                                Names_for_plot.append(Ename)
                                actual_ls.append(actual_l)
                                predicted_ls.append(pred_l)
                            else:
                                continue
                
                Element = "Calcium"
                plt.figure()
                mean_for_plot = np.mean(results_relative_for_print)
                # print("Mean for plot", mean_for_plot)
                # print(results_relative_for_print)
                plt.title("Using Neural Network"+"Neurons:"+str(neuron)+ "\nMean Relative Non-Zero Error "+str(Element)+": "+ str(np.round(mean_for_plot,2)))
                
                plt.scatter(Names_for_plot,results_relative_for_print, label = "Names and Relative Errors")
                plt.xlabel('Experiment Names')
                plt.ylabel("Relative Error")
                plt.grid()
                plt.legend()
                plt.xticks(rotation=45, ha='right')
                plt.tight_layout()
                results_relative_for_print = np.array(results_relative_for_print)
    
                results_folder = r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\ResultsThinLayer'
                if save:
                    plt.savefig(os.path.join(results_folder, str(Element)))
                plt.show()
                
                arr_a = np.array(actual_ls)
                arr_p = np.array(predicted_ls)
                # print(arr_a.flatten())
                
                if len(np.shape(arr_a)) == 3: #Doing all
                    arr_a = arr_a.flatten()
                    arr_p = arr_p.flatten()
                    Number_of_LS = 4
                if len(np.shape(arr_a) )== 2: #Only one LS
                    arr_a = arr_a.flatten()
                    arr_p = arr_p.flatten()
                    Number_of_LS = 1
                
                # print("arr_a:", arr_a)
                # print("arr_a shape:", arr_a.shape)
                # print("arr_p:", arr_p)
                # print("arr_p shape:", arr_p.shape)
                
    
    
                print("R2 Scores:")
                
                if Number_of_LS == 1:
                    LS_values = LS_Value
                    print("LS Value is: ",LS_values)
                    print(r2_score(arr_a,arr_p))
    
                    
                elif Number_of_LS == 4:
                    LS_Values = ['1','2','5','10']
                    for i in range(Number_of_LS):
                        print("LS",LS_Values[i])
                        print(r2_score(arr_a[i::Number_of_LS],arr_p[i::Number_of_LS]))
                print("All R2s")
                print(r2_score(arr_a.flatten(), arr_p.flatten()))
                R2_list.append(r2_score(arr_a.flatten(), arr_p.flatten()))
                mask =  arr_p.flatten() < 250
                print("Actuals less than 250:",r2_score(arr_a.flatten()[mask], arr_p.flatten()[mask]))
                print("NRMSE:",mean_squared_error(arr_a.flatten(), arr_p.flatten()))
                
                print("MAPE:",mean_absolute_percentage_error(arr_a.flatten(), arr_p.flatten()))
                Number_of_Experiments = int(len(arr_a)/Number_of_LS)
                Temp_factor = Number_of_LS
                arr_a_star, arr_p_star = arr_a.reshape(Number_of_Experiments,Number_of_LS),arr_p.reshape(Number_of_Experiments,Number_of_LS)
                Plot_errorbars(arr_a_star,arr_p_star,str(Element))
                
                
                Experiment_names_without_ename = Experiment_names[:Number_of_Experiments]
                colors = plt.cm.viridis(np.linspace(0, 10, Number_of_LS))
                colors = ['black','blue','orange','blue','green','grey'][:Number_of_LS]
                marker_styles = ['x', 'o', 's', 'D', '^']
                marker_styles = marker_styles[:Number_of_LS]
                plt.figure()
                
                actual = arr_a_star
                predicted = arr_p_star
                LS_names = ['1','2','5','10']
                plt.title("Neural Network by LS")
                for i in range(Number_of_LS):
                    
                    plt.scatter(actual[:,i], predicted[:,i], marker='x', color=colors[i], label="LS: "+str(LS_names[i]))
                
                plt.plot([min(arr_a), max(arr_a)], [min(arr_a), max(arr_a)], color='black', linestyle='--', linewidth=2)
                    # plt.scatter(gactual,gpredicted,marker='x', color='blue', label='Real data')
                plt.ylabel("Predicted values")
                plt.xlabel("Actual values")
                plt.grid(True)
                plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=3)
                plt.show()
                
                plt.figure()
                
    
                
                columns = ["Experiment Name", "R2_real", "R2_generated", "R2_Leftout", "MSE_real", "MSE_generated", "MSE_Leftout", "wmap_real", "wmap_generated", "WMAP_Leftout", "Rel_real", "Rel_generated", "Rel_Leftout"]
                df = pd.DataFrame(results, columns=columns)
                
                
                # Save the DataFrame to an Excel file
                if save:
                    df.to_excel("experiment_results_" + str(Element) +"noise_"+str(add_noise)+".xlsx", index=False)
                
                model_name = "Neural Network"
                LS_Names_mean = ['1','2','5','10']
                Ltruelist = arr_a_star
                Lpredlist = arr_p_star
                Relative_for_plot_LS = np.zeros(Number_of_LS)
                for i in range(Number_of_LS):
                    Relative_for_plot_LS[i] = Relative_loss(Ltruelist[:,i], Lpredlist[:,i])
                    
                print("Mean relative without highest three:", np.mean(ignore_highest_n_numbers(results_relative_for_print, N=3)))
                print(ignore_highest_n_numbers(results_relative_for_print, N=4))
                
                plt.figure()
                plt.title("Using Neural Network"+"Neurons:"+str(neuron) + "\nMean Relative Error "+str(Element)+": "+ str(np.round(mean_for_plot,2)))
                plt.scatter(LS_Names_mean,Relative_for_plot_LS, label = "Names and Relative Errors"+model_name)
                plt.xlabel('LS-Values')
                plt.ylabel("Relative Error")
                plt.grid()
                plt.legend()
                plt.xticks(rotation=45, ha='right')
                plt.tight_layout()
                results_relative_for_print = np.array(results_relative_for_print)
                results_folder = r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Results'
                if save:
                    plt.savefig(os.path.join(results_folder, str(Element),model_name))
                plt.show()
                
    
                Relative_for_plot_experiments = np.zeros(Number_of_Experiments)
                for i in range(Number_of_Experiments):
                    Relative_for_plot_experiments[i] = Relative_loss(Ltruelist[i,:], Lpredlist[i,:])
                    
                plt.figure()
                plt.title("Using Neural Network"+"Neurons:"+str(neuron)+"\nMean Relative Non-Zero Error "+str(Element)+": "+ str(np.round(mean_for_plot,2)))
                plt.scatter(Names_for_plot,Relative_for_plot_experiments, label = "Names and Relative Errors"+model_name)
                plt.xlabel('Experiment Names')
                plt.ylabel("Relative Error")
                plt.grid()
                plt.legend()
                plt.xticks(rotation=45, ha='right')
                plt.tight_layout()
                results_relative_for_print = np.array(results_relative_for_print)
                results_folder = r'C:\Users\willy\OneDrive\Desktop\Masters_Thesis\willy_column\Results'
                if save:
                    plt.savefig(os.path.join(results_folder, str(Element),model_name))
                plt.show()
                
                plt.figure()
                plt.title("Plot by experiment, mean for "+str(Element)+": "+ str(np.round(mean_for_plot,2)) + "\n#N: "+str(neuron))
                for index,name in enumerate(Names_for_plot):
                    plt.scatter(Ltruelist[index,:], Lpredlist[index,:], label = name)
                plt.grid(True)
                plt.xlabel("True")
                plt.ylabel("Predicted")
                min_val = min(np.min(Ltruelist), np.min(Lpredlist))
                max_val = max(np.max(Ltruelist), np.max(Lpredlist))
                plt.plot([min_val, max_val], [min_val, max_val], 'k--', lw=2, label='y = x')
                plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=3)
                plt.show()
                
                means_list.append(np.round(mean_for_plot,2))
        print(means_list)
        print(R2_list)
        print(neurons)
        